namespace FiremanSystem
{
    partial class PrzegladFOrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.przeglad1 = new FiremanSystem.Przeglad();
            this.nowyPrzeglad1 = new FiremanSystem.NowyPrzeglad();
            this.SuspendLayout();
            // 
            // przeglad1
            // 
            this.przeglad1.Dock = System.Windows.Forms.DockStyle.Top;
            this.przeglad1.Location = new System.Drawing.Point(0, 201);
            this.przeglad1.Name = "przeglad1";
            this.przeglad1.Size = new System.Drawing.Size(418, 338);
            this.przeglad1.TabIndex = 1;
            // 
            // nowyPrzeglad1
            // 
            this.nowyPrzeglad1.Dock = System.Windows.Forms.DockStyle.Top;
            this.nowyPrzeglad1.IDFIRMY = 0;
            this.nowyPrzeglad1.IDTypPrzegladu = 0;
            this.nowyPrzeglad1.Location = new System.Drawing.Point(0, 0);
            this.nowyPrzeglad1.Name = "nowyPrzeglad1";
            this.nowyPrzeglad1.NazwaFirmy = null;
            this.nowyPrzeglad1.Size = new System.Drawing.Size(418, 201);
            this.nowyPrzeglad1.TabIndex = 0;
            // 
            // PrzegladFOrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(418, 551);
            this.Controls.Add(this.przeglad1);
            this.Controls.Add(this.nowyPrzeglad1);
            this.Name = "PrzegladFOrm";
            this.Text = "Przeglad";
            this.ResumeLayout(false);

        }

        #endregion

        public NowyPrzeglad nowyPrzeglad1;
        public Przeglad przeglad1;

    }
}