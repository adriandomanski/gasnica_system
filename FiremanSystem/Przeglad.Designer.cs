namespace FiremanSystem
{
    partial class Przeglad
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtpMiesiacPrzegladu = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tboxNrProtokolu = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tboxNrFaktury = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpDataFaktury = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tboxUwagi = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnSave = new DevComponents.DotNetBar.ButtonX();
            this.label8 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.dtpDataPrzegladu = new System.Windows.Forms.DateTimePicker();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtpMiesiacPrzegladu
            // 
            this.dtpMiesiacPrzegladu.CustomFormat = "yyyy MMMM";
            this.dtpMiesiacPrzegladu.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMiesiacPrzegladu.Location = new System.Drawing.Point(124, 29);
            this.dtpMiesiacPrzegladu.Name = "dtpMiesiacPrzegladu";
            this.dtpMiesiacPrzegladu.Size = new System.Drawing.Size(284, 20);
            this.dtpMiesiacPrzegladu.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Miesi�c planowanego\r\nprzegl�du";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Numer Protoko�u";
            // 
            // tboxNrProtokolu
            // 
            this.tboxNrProtokolu.Location = new System.Drawing.Point(121, 0);
            this.tboxNrProtokolu.Name = "tboxNrProtokolu";
            this.tboxNrProtokolu.Size = new System.Drawing.Size(284, 20);
            this.tboxNrProtokolu.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Numer Faktury";
            // 
            // tboxNrFaktury
            // 
            this.tboxNrFaktury.Location = new System.Drawing.Point(121, 26);
            this.tboxNrFaktury.Name = "tboxNrFaktury";
            this.tboxNrFaktury.Size = new System.Drawing.Size(284, 20);
            this.tboxNrFaktury.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Data Faktury";
            // 
            // dtpDataFaktury
            // 
            this.dtpDataFaktury.Location = new System.Drawing.Point(121, 52);
            this.dtpDataFaktury.Name = "dtpDataFaktury";
            this.dtpDataFaktury.Size = new System.Drawing.Size(284, 20);
            this.dtpDataFaktury.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Nazwa firmy";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(121, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Nazwa firmy";
            // 
            // tboxUwagi
            // 
            this.tboxUwagi.Location = new System.Drawing.Point(124, 55);
            this.tboxUwagi.Multiline = true;
            this.tboxUwagi.Name = "tboxUwagi";
            this.tboxUwagi.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tboxUwagi.Size = new System.Drawing.Size(284, 101);
            this.tboxUwagi.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 79);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Uwagi";
            // 
            // btnSave
            // 
            this.btnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSave.Location = new System.Drawing.Point(335, 309);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(73, 23);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Zapisz";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 160);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Zrobiony";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(124, 160);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 4;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.tboxNrFaktury);
            this.panel1.Controls.Add(this.dtpDataFaktury);
            this.panel1.Controls.Add(this.dtpDataPrzegladu);
            this.panel1.Controls.Add(this.tboxNrProtokolu);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Location = new System.Drawing.Point(3, 186);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(411, 117);
            this.panel1.TabIndex = 5;
            this.panel1.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 84);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Data Przegl�du";
            // 
            // dtpDataPrzegladu
            // 
            this.dtpDataPrzegladu.Location = new System.Drawing.Point(121, 80);
            this.dtpDataPrzegladu.Name = "dtpDataPrzegladu";
            this.dtpDataPrzegladu.Size = new System.Drawing.Size(284, 20);
            this.dtpDataPrzegladu.TabIndex = 1;
            // 
            // Przeglad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tboxUwagi);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dtpMiesiacPrzegladu);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Name = "Przeglad";
            this.Size = new System.Drawing.Size(423, 339);
            this.Load += new System.EventHandler(this.Przeglad_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpMiesiacPrzegladu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tboxNrProtokolu;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tboxNrFaktury;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpDataFaktury;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tboxUwagi;
        private System.Windows.Forms.Label label7;
        private DevComponents.DotNetBar.ButtonX btnSave;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker dtpDataPrzegladu;
        private System.Windows.Forms.Label label9;
    }
}
