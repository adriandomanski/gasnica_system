using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using Microsoft.Office.Interop.Excel;
using System.Reflection;
using DevComponents.DotNetBar;
using Appl;
using DevComponents.DotNetBar.Controls;
using System.Diagnostics;


namespace FiremanSystem
{
    public partial class FiremanSystemForm : DevComponents.DotNetBar.Office2007Form
    {
        public OleDbConnection database;
        DataGridViewButtonColumn editButton;
        DataGridViewButtonColumn deleteButton;
        int movieIDInt;

        #region Form1 constructor
        public FiremanSystemForm()
        {

            InitializeComponent();
            TYPPrzegladu tp;
            tp = new TYPPrzegladu();
            tp.ID = "1";
            tp.NAME = "Roczny";

            combTyp.Items.Add(tp);
            tp = new TYPPrzegladu();
            tp.ID = "2";
            tp.NAME = "P�roczny";

            combTyp.Items.Add(tp);
            // iniciate DB connection
            string connectionString = FiremanSystem.Properties.Settings.Default.databaseConnectionString;// "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=database.mdb";
            try
            {

                database = new OleDbConnection(connectionString);
                database.Open();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }
        #endregion

        #region Load dataGrid
        public void loadDataGrid(string sqlQueryString)
        {

            OleDbCommand SQLQuery = new OleDbCommand();
            System.Data.DataTable data = null;
            dataGridView1.DataSource = null;
            SQLQuery.Connection = null;
            OleDbDataAdapter dataAdapter = null;
            dataGridView1.Columns.Clear(); // <-- clear columns
            //---------------------------------
            SQLQuery.CommandText = sqlQueryString;
            SQLQuery.Connection = database;
            data = new System.Data.DataTable();
            dataAdapter = new OleDbDataAdapter(SQLQuery);
            dataAdapter.Fill(data);
            //    data.Columns.Add("lp");
            dataGridView1.DataSource = data;
            dataGridView1.AllowUserToAddRows = false; // remove the null line
            dataGridView1.ReadOnly = true;
            dataGridView1.Columns[0].Visible = false;
            // insert delete button to datagridview
            deleteButton = new DataGridViewButtonColumn();
            deleteButton.HeaderText = "Usu�";
            deleteButton.Text = "Usu�";
            deleteButton.UseColumnTextForButtonValue = true;
            deleteButton.Width = 70;
            dataGridView1.Columns.Add(deleteButton);
            foreach (DataGridViewColumn var in dataGridView1.Columns)
            {
                var.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
        }
        #endregion

        public void loadDataGrid(string sqlQueryString, System.Windows.Forms.DataGridView datagree)
        {

            OleDbCommand SQLQuery = new OleDbCommand();
            System.Data.DataTable data = null;
            datagree.DataSource = null;
            SQLQuery.Connection = null;
            OleDbDataAdapter dataAdapter = null;
            datagree.Columns.Clear(); // <-- clear columns
            //---------------------------------
            SQLQuery.CommandText = sqlQueryString;
            SQLQuery.Connection = database;
            data = new System.Data.DataTable();
            dataAdapter = new OleDbDataAdapter(SQLQuery);
            dataAdapter.Fill(data);



            datagree.DataSource = data;

            datagree.AllowUserToAddRows = false; // remove the null line
            datagree.ReadOnly = true;

            datagree.Columns[0].Visible = false;
            datagree.Columns[1].Visible = false;
            datagree.Columns[7 + 1].Visible = false;
            datagree.Columns[8 + 1].Visible = false;
            datagree.Columns[12 + 1].Visible = false;
            datagree.Columns[13 + 1].Visible = false;
            datagree.Columns[14 + 1].Visible = false;
            datagree.Columns[15 + 1].Visible = false;
            datagree.Columns[2].Visible = false;//.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;// = 50;
            datagree.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;//.Width = 50;
            datagree.Columns[3 + 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;//.Width = 50;
            datagree.Columns[4 + 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
            datagree.Columns[5 + 1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;//.Width = 200;
            datagree.Columns[6 + 1].Width = 180;
            datagree.Columns[9 + 1].Width = 110;
            datagree.Columns[10 + 1].Width = 60;
            datagree.Columns[11 + 1].Width = 90;

            // insert edit button into datagridview
            editButton = new DataGridViewButtonColumn();
            editButton.HeaderText = "Edytuj / Podgl�d";
            editButton.Text = "Edytuj / Podgl�d";
            editButton.UseColumnTextForButtonValue = true;

            editButton.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;//
            deleteButton = new DataGridViewButtonColumn();
            deleteButton.HeaderText = "Usu�";
            deleteButton.Text = "Usu�";
            deleteButton.UseColumnTextForButtonValue = true;
            deleteButton.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
        }

        private void izlazToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        #region Close database connection
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Czy napewno chcesz zamkn�� program?", "Pytanie", MessageBoxButtons.YesNo))
            {
                database.Close();
            }
            else
            {
                e.Cancel = true;
            }
        }
        #endregion

        #region refresh button
        private void button2_Click(object sender, EventArgs e)
        {
            tboxNazwaFirmy.Clear();
            string queryString = "SELECT T.ID, T.NAZWA as Firma, T.ULICA as Ulica, T.NR_DOMU as nr_domu, T.NR_MIESZKANIA as nr_mieszkania, T.MIASTO as Miasto ,T.KOD_POCZTOWY as Kod_Pocztowy,T.NIP , TP.NAZWA as Typ_Przegladu, TP.ID   FROM TYP_PRZEGLADU TP ,FIRMA T WHERE TP.ID = T.ID_TYPU_PRZEGLADU";

            loadDataGrid(queryString);
            dataGridView1.Columns[9].Visible = false;
        }
        #endregion

        #region Input
        private void button6_Click(object sender, EventArgs e)
        {
            bool valid = true;
            string msg = "";

            foreach (Control var in panelNewFirm.Controls)
            {
                if (var.Text == "" && var.Tag != null)
                {
                    msg += var.Tag.ToString();
                    valid = false;
                }
            }
            foreach (Control var in panel2.Controls)
            {
                if (var.Text == "" && var.Tag != null)
                {
                    msg += var.Tag.ToString();
                    valid = false;
                }
            }
            if (valid)
            {
                string SQLString = "";
                string str1 = "temp" + DateTime.Now.Millisecond.ToString();



                SQLString = "INSERT INTO FIRMA(NAZWA, ULICA, MIASTO,KOD_POCZTOWY,NIP, ID_TYPU_PRZEGLADU, FAKTURA_NAZWA, FAKTURA_ULICA,  FAKTURA_MIASTO,  FAKTURA_KOD_POCZTOWY) VALUES('"
                            + tboxNazwa.Text + "','" + tboxUlica.Text + "','"
                            + tboxMiasto.Text + "','" + tboxKodPocztowy.Text + "','" + tboxNIP.Text + "','" +

                             ((TYPPrzegladu)combTyp.SelectedItem).ID + "','" + tboxFakturaNazwa.Text + "','" + tboxFakturaUlica.Text + "','" + tboxFakturaMiasto.Text + "','" + tboxFakturaKod.Text
                            + "');";

                OleDbCommand SQLCommand = new OleDbCommand();
                SQLCommand.CommandText = SQLString;
                SQLCommand.Connection = database;
                int response = -1;
                try
                {
                    response = SQLCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }



                if (response >= 1) MessageBox.Show("Firma Zapisana do Bazy", "Sukces", MessageBoxButtons.OK, MessageBoxIcon.Information);
                {
                    foreach (Control var in panelNewFirm.Controls)
                    {
                        var.Text = "";
                    }
                    foreach (Control var in panel2.Controls)
                    {
                        var.Text = "";
                    }
                }
            }
            else
            {
                MessageBox.Show(msg);
            }
        }

        public int CheckYear(string year)
        {
            int yr = int.Parse(year);
            if (yr >= 2100 || yr <= 1900)
            {
                return 1;
            }
            else
            {
                return yr;
            }
        }

        #endregion

        #region Delete/Edit button handling
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                return;
            }
            string queryString = "SELECT T.ID, T.NAZWA, T.ULICA, T.NR_DOMU, T.NR_MIESZKANIA, T.MIASTO,T.KOD_POCZTOWY,T.NIP, TP.NAZWA, TP.ID FROM TYP_PRZEGLADU TP ,FIRMA T WHERE TP.ID = T.ID_TYPU_PRZEGLADU";

            int currentRow = int.Parse(e.RowIndex.ToString());
            try
            {
                string movieIDString = dataGridView1[0, currentRow].Value.ToString();
                movieIDInt = int.Parse(movieIDString);
            }
            catch (Exception ex) { }

            if (dataGridView1.Columns[e.ColumnIndex] == deleteButton && currentRow >= 0)
            {
                if (DialogResult.Yes == DevComponents.DotNetBar.MessageBoxEx.Show("Czy napewno chcesz usun��?", "Pytanie", MessageBoxButtons.YesNo))
                {
                    // delete sql query
                    string queryDeleteString = "DELETE FROM FIRMA where ID = " + movieIDInt + "";
                    OleDbCommand sqlDelete = new OleDbCommand();
                    sqlDelete.CommandText = queryDeleteString;
                    sqlDelete.Connection = database;
                    sqlDelete.ExecuteNonQuery();
                    loadDataGrid(queryString);
                }
            }

        }
        #endregion

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        #region search by title
        private void button1_Click(object sender, EventArgs e)
        {
            //string title = textBox4.Text.ToString();
            //if (title != "")
            //{
            //    string queryString = "SELECT movieID, Title, Publisher, Previewed, MovieYear, Type FROM movie,movietype WHERE movietype.typeID = movie.typeID AND movie.title LIKE '" + title + "%'";
            string queryString = "SELECT  T.ID,'' as lp, T.NAZWA as Firma, T.ULICA as Adres, "
            + "T.NR_DOMU as nr_domu, T.NR_MIESZKANIA as nr_mieszkania, T.MIASTO as Miasto " +
            ",T.KOD_POCZTOWY as Kod_Pocztowy,T.NIP , TP.NAZWA as Typ_Przegladu, TP.ID,  T.FAKTURA_NAZWA, "
            + "T.FAKTURA_ULICA,  T.FAKTURA_MIASTO, T.FAKTURA_NR_DOMU, T.FAKTURA_KOD_POCZTOWY, "
            + " T.FAKTURA_NR_MIESZKANIA FROM TYP_PRZEGLADU TP ,FIRMA T "
            + "WHERE TP.ID = T.ID_TYPU_PRZEGLADU AND LCASE(T.NAZWA) LIKE '%" + tboxNazwaFirmy.Text.ToLower() + "%'";

            loadDataGrid(queryString);
            try
            {
                foreach (DataGridViewColumn var in dataGridView1.Columns)
                {
                    var.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                }
                dataGridView1.Columns[1].Width = 30;
                dataGridView1.Columns[1 + 1].Width = 250;
                dataGridView1.Columns[2 + 1].Width = 230;
                dataGridView1.Columns[5 + 1].Width = 160;
                dataGridView1.Columns[6 + 1].Width = 80;
                dataGridView1.Columns[7 + 1].Width = 85;
                dataGridView1.Columns[8 + 1].Width = 76;
                dataGridView1.Columns[16 + 1].Width = 45;//  = DataGridViewAutoSizeColumnMode.DisplayedCells;
                dataGridView1.Columns[3 + 1].Visible = false;
                dataGridView1.Columns[4 + 1].Visible = false;
                dataGridView1.Columns[9 + 1].Visible = false;
                dataGridView1.Columns[10 + 1].Visible = false;
                dataGridView1.Columns[11 + 1].Visible = false;
                dataGridView1.Columns[12 + 1].Visible = false;
                dataGridView1.Columns[13 + 1].Visible = false;
                dataGridView1.Columns[14 + 1].Visible = false;
                dataGridView1.Columns[15 + 1].Visible = false;
                setSort();
            }
            catch (Exception)
            {

                //throw;
            }
            //}
            //else
            //{
            //    MessageBox.Show("You muste enter movie title","Warning",MessageBoxButtons.OK,MessageBoxIcon.Warning);
            //}
        }
        #endregion


        #region search by year
        private void button4_Click(object sender, EventArgs e)
        {
            string firstYear = textBox5.Text.ToString();
            string secondYear = textBox6.Text.ToString(); ;
            int yr1 = CheckYear(firstYear);
            int yr2 = CheckYear(secondYear);
            string queryString = "SELECT T.ID, T.NAZWA, T.ULICA, T.NR_DOMU, T.NR_MIESZKANIA, T.MIASTO,T.KOD_POCZTOWY,T.NIP, TP.NAZWA FROM TYP_PRZEGLADU TP ,FIRMA T WHERE TP.ID = T.ID_TYPU_PRZEGLADU";

            loadDataGrid(queryString);

        }
        #endregion

        #region search previewed movies
        private void button3_Click(object sender, EventArgs e)
        {

            string queryString = "SELECT T.ID, T.NAZWA, T.ULICA, T.NR_DOMU, T.NR_MIESZKANIA, T.MIASTO,T.KOD_POCZTOWY,T.NIP, TP.NAZWA  FROM TYP_PRZEGLADU TP ,FIRMA T WHERE TP.ID = T.ID_TYPU_PRZEGLADU";

            loadDataGrid(queryString);
        }
        #endregion

        private void button6_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button6_Click(null, null);
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string queryString = "SELECT T.ID, T.NAZWA, T.ULICA, T.NR_DOMU, T.NR_MIESZKANIA, T.MIASTO,T.KOD_POCZTOWY,T.NIP, TP.NAZWA, TP.ID FROM TYP_PRZEGLADU TP ,FIRMA T WHERE TP.ID = T.ID_TYPU_PRZEGLADU";

            loadDataGrid(queryString);
            dataGridView1.Columns[9].Visible = false;

        }

        private void button7_Click(object sender, EventArgs e)
        {
            tabControl1_SelectedIndexChanged(null, null);
        }

        private void tabItem2_Click(object sender, EventArgs e)
        {
            //button1.PerformClick();
        }

        private void btn_Search_Click(object sender, EventArgs e)
        {
            string dataOd = dtpRok.Value.Year.ToString() + "-" + (dtpMiesiac.Value.Month < 10 ? "0" + dtpMiesiac.Value.Month.ToString() : dtpMiesiac.Value.Month.ToString()) + "-01";
            int iloscDni = DateTime.DaysInMonth(dtpRok.Value.Year, dtpMiesiac.Value.Month);
            string dataDo = dtpRok.Value.Year.ToString() + "-" +
                ((dtpMiesiac.Value.Month < 10 ?
                "0" + dtpMiesiac.Value.Month.ToString() : dtpMiesiac.Value.Month.ToString())
                + "-" + (iloscDni < 10 ? "0" + iloscDni.ToString() : iloscDni.ToString()));



            string queryString = "SELECT " +
                " t1.* "
            + " FROM [SELECT FIRMA.ID as firmaid,   PRZEGLAD.ID, "
            + "PRZEGLAD.MIESIAC_PRZEGLADU as miesiacPrzegladu,  '' as lp, "
            + "PRZEGLAD.DATA_PRZEGLADU as DataPrzegladu, PRZEGLAD.ZROBIONY as ZROBIONY, "
            + "FIRMA.NAZWA, FIRMA.ULICA as Adres, FIRMA.NR_DOMU, FIRMA.NR_MIESZKANIA, "
            + "FIRMA.MIASTO, FIRMA.KOD_POCZTOWY as KOD, FIRMA.NIP, PRZEGLAD.DATA_FAKTURY, "
            + "PRZEGLAD.NR_PROTOKOlU, PRZEGLAD.NR_FAKTURY , PRZEGLAD.UWAGI "
            + " FROM FIRMA INNER JOIN PRZEGLAD ON FIRMA.ID = PRZEGLAD.ID_FIRMA]. AS t1"
            + " WHERE t1.miesiacPrzegladu>=#" + dataOd + "# and t1.miesiacPrzegladu<=#" + dataDo + "# "

            + " ";


            if (cbDoWykonania.Checked)
            {
                queryString += "and t1.ZROBIONY=false;";
                lblPrzeglady.Text = "Przegl�dy do wykonania okresie w: " + dtpMiesiac.Text + " " + dtpRok.Text;
            }
            if (cbWykonane.Checked)
            {
                queryString += "and t1.ZROBIONY=true;";
                lblPrzeglady.Text = "Przegl�dy wykonane okresie w: " + dtpMiesiac.Text + " " + dtpRok.Text;
            }
            if (cbWszystkie.Checked)
            {
                queryString += ";";
                lblPrzeglady.Text = "Wszystkie przeg�dy w okresie: " + dtpMiesiac.Text + " " + dtpRok.Text;
            }

            //  and pd.DATA_PRZEGLADU>=#" + dataOd + "# and pd.DATA_PRZEGLADU<=#" + dataDo + "#;";

            loadDataGrid(queryString, dgvPrzeglady);
            clearText();
            setSort2();

        }

        private void clearText()
        {
            foreach (DataGridViewRow var in dgvPrzeglady.Rows)
            {
                foreach (DataGridViewCell var1 in var.Cells)
                {
                    string str = var1.Value.ToString();
                    var1.Value = str;
                    var1.Style.WrapMode = DataGridViewTriState.True;
                }
            }
        }

        private void btn_showAll_Click(object sender, EventArgs e)
        {
            string queryString = "SELECT PRZEGLAD.ID, PRZEGLAD.MIESIAC_PRZEGLADU as miesiacPrzegladu,  PRZEGLAD.DATA_PRZEGLADU as DataPrzegladu, PRZEGLAD.ZROBIONY as ZROBIONY, FIRMA.NAZWA, FIRMA.ULICA, FIRMA.NR_DOMU, FIRMA.NR_MIESZKANIA, FIRMA.MIASTO, FIRMA.KOD_POCZTOWY, FIRMA.NIP FROM FIRMA INNER JOIN PRZEGLAD ON FIRMA.ID = PRZEGLAD.ID_FIRMA";// WHERE pd.DATA_PRZEGLADU>=#" + dataOd + "# and pd.DATA_PRZEGLADU<=#" + dataDo + "#;";

            loadDataGrid(queryString, dgvPrzeglady);
            lblPrzeglady.Text = "Wszystkie Przegl�dy";
        }

        private bool exportGrid(String fileName, System.Windows.Forms.DataGridView grid)
        {
            try
            {
                Dictionary<String, String> translations = new Dictionary<string, string>();
                System.Data.DataTable exportedTable = createTableToExport(translations, grid);
                fillTable(exportedTable, translations, grid);

                ExcelConvertor conv = new ExcelConvertor();
                conv.ShowExcelTableBorder = true;
                conv.Convert(exportedTable, fileName);

                return true;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("The process cannot access the file"))
                {
                    MessageBoxEx.Show("Wybrany plik jest otwarty przez inny program.\nWybierz inny plik lub zamknij program, kt�ry u�ywa obecny a nast�pnie pon�w pr�b�.");
                }
                return false;
            }
        }
        /// <summary>
        /// wypelnia DataTablea danymi z grida
        /// </summary>
        /// <param name="exportedTable"></param>
        private void fillTable(System.Data.DataTable exportedTable, Dictionary<String, String> translations, System.Windows.Forms.DataGridView grid)
        {
            foreach (DataGridViewRow row in grid.Rows)
            {
                DataRow dr = getValyesFromRow(exportedTable, row, translations);
                exportedTable.Rows.Add(dr);
            }
        }
        /// <summary>
        /// skopiuje wartosci z DataGridViewRow do DataRow
        /// </summary>
        /// <param name="row"></param>
        /// <param name="translations"></param>
        /// <returns></returns>
        private DataRow getValyesFromRow(System.Data.DataTable exportedTable, DataGridViewRow row, Dictionary<string, string> translations)
        {
            DataRow dr = exportedTable.NewRow();

            foreach (String key in translations.Keys)
            {
                if (row.Cells[key] == null || row.Cells[key].Value == null)
                    continue;
                String value = "";
                if (row.Cells[key].Value is bool)
                    value = getStringFromBool((bool)row.Cells[key].Value);
                else
                    value = row.Cells[key].Value.ToString();
                String translatedKey = translations[key];
                dr[translatedKey] = value;
            }
            return dr;
        }
        /// <summary>
        /// towrzy obiekt DataTable na podstawie kolumn, ktore sa w gridzie
        /// </summary>
        /// <returns></returns>
        private System.Data.DataTable createTableToExport(Dictionary<String, String> translations, System.Windows.Forms.DataGridView grid)
        {
            System.Data.DataTable exportedDataTable = new System.Data.DataTable();

            foreach (DataGridViewColumn column in grid.Columns)
            {
                if (!column.Visible)
                    continue;
                if (column is DataGridViewImageColumn)
                    continue;
                exportedDataTable.Columns.Add(getColumnName(column, translations));
            }
            return exportedDataTable;
        }

        private string getColumnName(DataGridViewColumn column, Dictionary<string, string> translations)
        {
            String translatedValue = column.HeaderText;

            foreach (String val in translations.Values)
            {
                if (val == translatedValue)
                    translatedValue = generateNewTranslatedValue(val, translations);
            }

            translations[column.Name] = translatedValue;
            return translatedValue;
        }

        private string generateNewTranslatedValue(string header, Dictionary<string, string> translations)
        {
            bool generationSucceed = false;

            int valuePrafix = 1;
            String potentialNewValue = header + "v" + valuePrafix;

            bool continueBigLoop = false;

            do//bigLoop
            {
                continueBigLoop = false;
                foreach (String subVal in translations.Values)
                {
                    if (subVal == potentialNewValue)
                    {
                        ++valuePrafix;
                        potentialNewValue = header + "v" + valuePrafix;
                        generationSucceed = false;
                        continueBigLoop = true;
                        break;
                    }
                }
                if (continueBigLoop)
                    continue;
                generationSucceed = true;

            } while (!generationSucceed);

            return potentialNewValue;
        }
        private String getStringFromBool(bool val)
        {
            return val ? "Tak" : "Nie";
        }

        public static void openFile(string path)
        {
            try
            {
                Process.Start(path);
            }
            catch (Exception ex)
            {
                Process.Start("rundll32.exe", "shell32.dll,OpenAs_RunDLL " + path);
            }
        }

        private void btnDrukuj_Click(object sender, EventArgs e)
        {
            SaveFileDialog fileDialog = new SaveFileDialog();
            fileDialog.CheckPathExists = true;
            fileDialog.AddExtension = true;
            fileDialog.Filter = " XLS|*.xls";
            fileDialog.Title = "Zapis pliku do wydruku";
            fileDialog.ShowDialog();
            if (fileDialog.FileName == "")
                return;

            if (exportGrid(fileDialog.FileName, dgvPrzeglady))
            {
                MessageBoxEx.Show("Zapis przebieg� pomy�lnie", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                openFile(fileDialog.FileName);
            }
            else
            {
                MessageBoxEx.Show("Zapis nie powi�d� si�", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return;

            object objApp_Late;
            object objBook_Late;
            object objBooks_Late;
            object objSheets_Late;
            object objSheet_Late;
            object objRange_Late;
            object[] Parameters;
            string[] headers = new string[dgvPrzeglady.ColumnCount - 1];
            string[] columns = new string[dgvPrzeglady.ColumnCount - 1];

            int i = 0;
            int c = 0;
            for (c = 0; c < dgvPrzeglady.ColumnCount - 1; c++)
            {
                headers[c] = dgvPrzeglady.Rows[0].Cells[c].OwningColumn.Name.ToString();
                i = c + 65;
                columns[c] = Convert.ToString((char)i);
            }

            try
            {
                // Get the class type and instantiate Excel.
                Type objClassType;
                objClassType = Type.GetTypeFromProgID("Excel.Application");
                objApp_Late = Activator.CreateInstance(objClassType);
                //Get the workbooks collection.
                objBooks_Late = objApp_Late.GetType().InvokeMember("Workbooks",
                BindingFlags.GetProperty, null, objApp_Late, null);
                //Add a new workbook.
                objBook_Late = objBooks_Late.GetType().InvokeMember("Add",
                BindingFlags.InvokeMethod, null, objBooks_Late, null);
                //Get the worksheets collection.
                objSheets_Late = objBook_Late.GetType().InvokeMember("Worksheets",
                BindingFlags.GetProperty, null, objBook_Late, null);
                //Get the first worksheet.
                Parameters = new Object[1];
                Parameters[0] = 1;
                objSheet_Late = objSheets_Late.GetType().InvokeMember("Item",
                BindingFlags.GetProperty, null, objSheets_Late, Parameters);

                if (true)
                {
                    // Create the headers in the first row of the sheet
                    for (c = 0; c < dgvPrzeglady.ColumnCount - 1; c++)
                    {
                        //Get a range object that contains cell.
                        Parameters = new Object[2];
                        Parameters[0] = columns[c] + "1";
                        Parameters[1] = Missing.Value;
                        objRange_Late = objSheet_Late.GetType().InvokeMember("Range",
                        BindingFlags.GetProperty, null, objSheet_Late, Parameters);
                        //Write Headers in cell.
                        Parameters = new Object[1];
                        Parameters[0] = headers[c];
                        objRange_Late.GetType().InvokeMember("Value", BindingFlags.SetProperty,
                        null, objRange_Late, Parameters);
                    }
                }

                // Now add the data from the grid to the sheet starting in row 2
                for (i = 0; i < dgvPrzeglady.RowCount; i++)
                {
                    for (c = 0; c < dgvPrzeglady.ColumnCount - 1; c++)
                    {
                        //Get a range object that contains cell.
                        Parameters = new Object[2];
                        Parameters[0] = columns[c] + Convert.ToString(i + 2);
                        Parameters[1] = Missing.Value;
                        objRange_Late = objSheet_Late.GetType().InvokeMember("Range",
                        BindingFlags.GetProperty, null, objSheet_Late, Parameters);
                        //Write Headers in cell.
                        Parameters = new Object[1];
                        Parameters[0] = (dgvPrzeglady.Rows[i].Cells[headers[c]].Value.ToString().ToLower() == "true" || dgvPrzeglady.Rows[i].Cells[headers[c]].Value.ToString().ToLower() == "false" ? ((bool)dgvPrzeglady.Rows[i].Cells[headers[c]].Value ? "tak" : "") : dgvPrzeglady.Rows[i].Cells[headers[c]].Value.ToString());
                        objRange_Late.GetType().InvokeMember("Value", BindingFlags.SetProperty,
                        null, objRange_Late, Parameters);
                    }
                }

                //Return control of Excel to the user.
                Parameters = new Object[1];
                Parameters[0] = true;
                objApp_Late.GetType().InvokeMember("Visible", BindingFlags.SetProperty,
                null, objApp_Late, Parameters);
                objApp_Late.GetType().InvokeMember("UserControl", BindingFlags.SetProperty,
                null, objApp_Late, Parameters);
            }
            catch (Exception theException)
            {
                String errorMessage;
                errorMessage = "Error: ";
                errorMessage = String.Concat(errorMessage, theException.Message);
                errorMessage = String.Concat(errorMessage, " Line: ");
                errorMessage = String.Concat(errorMessage, theException.Source);

                MessageBox.Show(errorMessage, "Error");
            }
        }

        private void dgvPrzeglady_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            return;
            if (e.RowIndex > -1 && e.ColumnIndex == editButton.Index)
            {
                PrzegladFOrm fr = new PrzegladFOrm();
                fr.TRYB = PrzegladFOrm.TrybENum.Edycja;
                fr.przeglad1.idFirma = int.Parse(dgvPrzeglady[1, e.RowIndex].Value.ToString());
                fr.przeglad1.dataPrzegladu = dgvPrzeglady[3 + 1, e.RowIndex].Value.ToString();
                fr.przeglad1.dataPrzegladuMiesiac = dgvPrzeglady[2, e.RowIndex].Value.ToString();
                fr.przeglad1.zrobiony = Convert.ToBoolean(dgvPrzeglady[4, e.RowIndex].Value.ToString());
                fr.przeglad1.nrProtokolu = dgvPrzeglady[13 + 1, e.RowIndex].Value.ToString();
                fr.przeglad1.nrFAktury = dgvPrzeglady[14 + 1, e.RowIndex].Value.ToString();
                fr.przeglad1.nazwaFirmy = dgvPrzeglady[5 + 1, e.RowIndex].Value.ToString();
                fr.przeglad1.uwagi = dgvPrzeglady[15 + 1, e.RowIndex].Value.ToString();
                fr.przeglad1.dataFaktury = dgvPrzeglady[12 + 1, e.RowIndex].Value.ToString();
                fr.przeglad1.idPrzeglad = int.Parse(dgvPrzeglady[0, e.RowIndex].Value.ToString());
                fr.przeglad1.SetDataToControl();
                fr.ShowDialog();
            }
            if (e.RowIndex > -1 && e.ColumnIndex == deleteButton.Index)
            {
                if (DialogResult.Yes == DevComponents.DotNetBar.MessageBoxEx.Show("Czy napewno chcesz usun��?", "Pytanie", MessageBoxButtons.YesNo))
                {
                    PrzegladFOrm fr = new PrzegladFOrm();
                    fr.TRYB = PrzegladFOrm.TrybENum.Edycja;
                    fr.ShowDialog();
                }
            }
        }

        private void tboxNazwa_TextChanged(object sender, EventArgs e)
        {

        }

        private void combTyp_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tboxUlica_TextChanged(object sender, EventArgs e)
        {

        }

        private void tboxMiasto_TextChanged(object sender, EventArgs e)
        {

        }

        private void tboxNrDomu_TextChanged(object sender, EventArgs e)
        {
            tboxFakturaNrDomu.Text.Replace("/", ".");
        }

        private void tboxKodPocztowy_TextChanged(object sender, EventArgs e)
        {

        }

        private void tboxNrMieszkania_TextChanged(object sender, EventArgs e)
        {

        }

        private void tboxNIP_TextChanged(object sender, EventArgs e)
        {

        }



        private void tboxNazwaFirmy_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1.PerformClick();
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
            {
                return;
            }
            int currentRow = int.Parse(e.RowIndex.ToString());
            try
            {
                string movieIDString = dataGridView1[0, currentRow].Value.ToString();
                movieIDInt = int.Parse(movieIDString);
            }
            catch (Exception ex) { }
            // edit button
            if (dataGridView1.Columns[e.ColumnIndex] != deleteButton && currentRow >= 0)
            {
                try
                {

                    string title = dataGridView1[1 + 1, currentRow].Value.ToString();
                    string publisher = dataGridView1[2 + 1, currentRow].Value.ToString();
                    string previewed = dataGridView1[3 + 1, currentRow].Value.ToString();
                    string year = dataGridView1[4 + 1, currentRow].Value.ToString();
                    string type = dataGridView1[5 + 1, currentRow].Value.ToString();

                    //T.ID, T.NAZWA, T.ULICA, T.NR_DOMU, T.NR_MIESZKANIA, T.MIASTO,T.KOD_POCZTOWY,T.NIP, TP.NAZWA
                    //runs form 2 for editing    
                    FirmaDaneForm f2 = new FirmaDaneForm();
                    f2.Saved += new FirmaDaneForm.SavedHandler(f2_Saved);
                    ID_FIRMSelected = int.Parse(dataGridView1[0, currentRow].Value.ToString());
                    f2.ID_FIRM = int.Parse(dataGridView1[0, currentRow].Value.ToString());
                    f2.NAZWA = dataGridView1[1 + 1, currentRow].Value.ToString();
                    f2.ULICA = dataGridView1[2 + 1, currentRow].Value.ToString();
                    f2.NR_DOMU = dataGridView1[3 + 1, currentRow].Value.ToString();
                    f2.NR_MIESZKANIA = dataGridView1[4 + 1, currentRow].Value.ToString();
                    f2.MIASTO = dataGridView1[5 + 1, currentRow].Value.ToString();
                    f2.KOD_POCZTOWY = dataGridView1[6 + 1, currentRow].Value.ToString();
                    f2.NIP = dataGridView1[7 + 1, currentRow].Value.ToString();
                    f2.ID_TYP_PRZEDLADU = dataGridView1[9 + 1, currentRow].Value.ToString();
                    f2.FAKTURA_NAZWA = dataGridView1[10 + 1, currentRow].Value.ToString();
                    f2.FAKTURA_ULICA = dataGridView1[11 + 1, currentRow].Value.ToString();
                    f2.FAKTURA_MIASTO = dataGridView1[12 + 1, currentRow].Value.ToString();
                    f2.FAKTURA_NR_DOMU = dataGridView1[13 + 1, currentRow].Value.ToString();
                    f2.FAKTURA_KOD_POCZTOWY = dataGridView1[14 + 1, currentRow].Value.ToString();
                    f2.FAKTURA_NR_MIESZKANIA = dataGridView1[15 + 1, currentRow].Value.ToString();

                    f2.ShowDialog();

                    dataGridView1.Update();

                }
                catch (Exception)
                {
                    return;
                }
            }
        }

        int ID_FIRMSelected = 0;
        void f2_Saved(object obj, EventArgs eventArgs)
        {
            button1_Click(null, null);
        }

        private void tboxFakturaNrDomu_TextChanged(object sender, EventArgs e)
        {
            tboxFakturaNrDomu.Text.Replace('/', '.');

        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            tboxFakturaNrDomu.Text.Replace('/', '.');
        }
        PrzegladFOrm fr;
        private void dgvPrzeglady_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {

                fr = new PrzegladFOrm();
                fr.Disposed += new EventHandler(fr_Disposed);
                fr.TRYB = PrzegladFOrm.TrybENum.Edycja;
                fr.przeglad1.idFirma = int.Parse(dgvPrzeglady[0, e.RowIndex].Value.ToString());
                fr.przeglad1.idPrzeglad = int.Parse(dgvPrzeglady[1, e.RowIndex].Value.ToString());
                fr.przeglad1.dataPrzegladu = dgvPrzeglady[3 + 1, e.RowIndex].Value.ToString();
                if (fr.przeglad1.dataPrzegladu == "0001-01-01 00:00:00")
                {
                    fr.przeglad1.dataPrzegladu = "";
                }
                fr.przeglad1.dataPrzegladuMiesiac = dgvPrzeglady[2, e.RowIndex].Value.ToString();
                fr.przeglad1.zrobiony = Convert.ToBoolean(dgvPrzeglady[4 + 1, e.RowIndex].Value.ToString());
                fr.przeglad1.nrProtokolu = dgvPrzeglady[13 + 1, e.RowIndex].Value.ToString();
                fr.przeglad1.nrFAktury = dgvPrzeglady[14 + 1, e.RowIndex].Value.ToString();
                fr.przeglad1.nazwaFirmy = dgvPrzeglady[5 + 1, e.RowIndex].Value.ToString();
                fr.przeglad1.uwagi = dgvPrzeglady[15 + 1, e.RowIndex].Value.ToString();
                fr.przeglad1.dataFaktury = dgvPrzeglady[12 + 1, e.RowIndex].Value.ToString();
                fr.przeglad1.SetDataToControl();
                fr.ShowDialog();

            }
        }

        void fr_Disposed(object sender, EventArgs e)
        {
            btn_Search_Click(null, null);
        }

        private void dgvPrzeglady_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
            {
                if (e.Shift)
                {
                    dgvPrzeglady.CurrentCell.Value += Environment.NewLine;
                    SendKeys.Send("{End}");
                }
            }
        }


        private void setSort()
        {
            foreach (DataGridViewRow var in dataGridView1.Rows)
            {
                var.Cells[1].Value = var.Index + 1;
            }
        }
        private void setSort2()
        {
            foreach (DataGridViewRow var in dgvPrzeglady.Rows)
            {
                var.Cells[3].Value = var.Index + 1;
            }
        }

        private void dataGridView1_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex != 1)
            {
                setSort();
            }
        }

        private void dgvPrzeglady_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex != 3)
            {
                setSort2();
            }
        }

    }
}