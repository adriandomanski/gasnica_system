using System;
using System.Data;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace Appl
{
    /// <summary>
    /// To generate excel file.
    /// </summary>
    public class ExcelConvertor
    {

        private Dictionary<String, String> aliases;//column name , alias

        /// <summary>
        /// pozwala ustawic aliasy,ktore bede uzywana w naglowkach kolumn
        /// </summary>
        public Dictionary<String, String> Aliases
        {
            get
            {
                return aliases;
            }
            set
            {
                aliases = value;
            }
        }

        public string Convert(DataTable oDataTable, string fullpath)
        {
            StreamWriter SW;
            
            //SW = File.CreateText(fullpath);
            SW = new StreamWriter(fullpath,false,System.Text.Encoding.GetEncoding("iso-8859-2"));


            StringBuilder oStringBuilder = new StringBuilder();

            /********************************************************
             * Start, check for border width
             * ******************************************************/
            int borderWidth = 0;

            if (_ShowExcelTableBorder)
            {
                borderWidth = 1;
            }
            /********************************************************
             * End, Check for border width
             * ******************************************************/

            /********************************************************
             * Start, Check for bold heading
             * ******************************************************/
            string boldTagStart = "";
            string boldTagEnd = "";
            if (_ExcelHeaderBold)
            {
                boldTagStart = "<B>";
                boldTagEnd = "</B>";
            }

            /********************************************************
             * End,Check for bold heading
             * ******************************************************/

            oStringBuilder.Append("<Table border=" + borderWidth + ">");

            /*******************************************************************
             * Start, Creating table header
             * *****************************************************************/

            oStringBuilder.Append("<TR>");
            if (aliases != null)
            {
                foreach (DataColumn oDataColumn in oDataTable.Columns)
                {
                    oStringBuilder.Append("<TD>" + boldTagStart + aliases[oDataColumn.ColumnName] + boldTagEnd + "</TD>");
                }
            }
            else
            {
                foreach (DataColumn oDataColumn in oDataTable.Columns)
                {
                    oStringBuilder.Append("<TD>" + boldTagStart + oDataColumn.ColumnName + boldTagEnd + "</TD>");
                }
            }

            oStringBuilder.Append("</TR>");

            /*******************************************************************
             * End, Creating table header
             * *****************************************************************/

            /*******************************************************************
             * Start, Creating rows
             * *****************************************************************/

            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                oStringBuilder.Append("<TR>");

                foreach (DataColumn oDataColumn in oDataTable.Columns)
                {
                    if (oDataRow[oDataColumn.ColumnName] is long)
                    {
                        string str = oDataRow[oDataColumn.ColumnName].ToString();
                        str = str.Replace("\r","<br>");
                        str = str.Replace("0001-01-01", "");
                        str = str.Replace("00:00:00", "");
                        oStringBuilder.Append("<TD align=right>" + str + "</TD>");
                    }
                    else
                    {
                        string str = oDataRow[oDataColumn.ColumnName].ToString();
                        str = str.Replace("\r", "<br>");
                        
                        str = str.Replace("0001-01-01", "");
                        str = str.Replace("00:00:00", "");
                        oStringBuilder.Append("<TD>" + str + "</TD>");
                    }


                }

                oStringBuilder.Append("</TR>");
            }


            /*******************************************************************
             * End, Creating rows
             * *****************************************************************/



            oStringBuilder.Append("</Table>");

            SW.WriteLine(oStringBuilder.ToString());
            SW.Close();

            return fullpath;
        }
        /// <summary>
        /// To generate excel file.
        /// </summary>
        /// <param name="oDataTable"></param>
        /// <param name="directoryPath"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public string Convert(DataTable oDataTable, string directoryPath, string fileName)
        {
            string fullpath = "";

            if (directoryPath.Substring(directoryPath.Length - 1, 1) == @"\" || directoryPath.Substring(directoryPath.Length - 1, 1) == "/")
            {
                fullpath = directoryPath + fileName;
            }
            else
            {
                fullpath = directoryPath + @"\" + fileName;
            }
            return Convert(oDataTable, fullpath);

        }

        private bool _ShowExcelTableBorder = false;

        /// <summary>
        /// To show or hide the excel table border
        /// </summary>
        public bool ShowExcelTableBorder
        {
            get
            {
                return _ShowExcelTableBorder;
            }
            set
            {
                _ShowExcelTableBorder = value;
            }
        }

        private bool _ExcelHeaderBold = true;


        /// <summary>
        /// To make header bold or normal
        /// </summary>
        public bool ExcelHeaderBold
        {
            get
            {
                return _ExcelHeaderBold;
            }
            set
            {
                ExcelHeaderBold = value;
            }
        }
    }
}
