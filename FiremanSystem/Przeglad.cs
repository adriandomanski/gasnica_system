using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace FiremanSystem
{
    public partial class Przeglad : UserControl
    {
        public string dataPrzegladu, dataPrzegladuMiesiac, uwagi, nrProtokolu, nrFAktury, dataFaktury, nazwaFirmy;
        public bool zrobiony;
        public int idFirma, idPrzeglad, idTypPrzegladu;

        public Przeglad()
        {
            InitializeComponent();
            dtpDataFaktury.Value = DateTime.Now;
            dtpMiesiacPrzegladu.Value = DateTime.Now;
        }
        public void SetDataToControl()
        {
            label6.Text = nazwaFirmy;
            tboxNrFaktury.Text = nrFAktury;
            tboxNrProtokolu.Text = nrProtokolu;
            tboxUwagi.Text = uwagi;
            try
            {
                dtpDataFaktury.Value = Convert.ToDateTime(dataFaktury);

            }
            catch (Exception)
            {
                
                //throw;
            }
            try
            {

                dtpMiesiacPrzegladu.Value = Convert.ToDateTime(dataPrzegladuMiesiac);
            }
            catch (Exception)
            {
                
             //   throw;
            }
            try
            {

                dtpDataPrzegladu.Value = Convert.ToDateTime(dataPrzegladu);
            }
            catch (Exception)
            {

                //   throw;
            }

            checkBox1.Checked = zrobiony;
        }

        private void Przeglad_Load(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            FiremanSystemForm f1;


            string SQLString = "";

            f1 = new FiremanSystemForm();
            string SQLUpdateString;
            string data = dtpMiesiacPrzegladu.Value.ToShortDateString();

            if (checkBox1.Checked)
            {
                SQLUpdateString = "UPDATE PRZEGLAD SET Miesiac_Przegladu = #" + dtpMiesiacPrzegladu.Value.Month.ToString() + "/" + dtpMiesiacPrzegladu.Value.Day.ToString() + "/" + dtpMiesiacPrzegladu.Value.Year.ToString() +
    "#,  DATA_PRZEGLADU = #" + dtpDataPrzegladu.Value.Month.ToString() + "/" + dtpDataPrzegladu.Value.Day.ToString() + "/" + dtpDataPrzegladu.Value.Year.ToString() +
    "#, DATA_FAKTURY= #" + dtpDataFaktury.Value.Month.ToString() + "/" + dtpDataFaktury.Value.Day.ToString() + "/" + dtpDataFaktury.Value.Year.ToString() +
    "#, UWAGI='" + tboxUwagi.Text.Replace("'", "''") +
    "', NR_PROTOKOLU= '" + tboxNrProtokolu.Text.Replace("'", "''") +
    "', NR_FAKTURY='" + tboxNrFaktury.Text.Replace("'", "''") +
    "', ZROBIONY= " + checkBox1.Checked.ToString() +
    " WHERE ID=" + idPrzeglad + ";";
            }
            else
            {
                SQLUpdateString = "UPDATE PRZEGLAD SET "+
                    "Miesiac_Przegladu = #" + dtpMiesiacPrzegladu.Value.Month.ToString() + "/" + dtpMiesiacPrzegladu.Value.Day.ToString() + "/" + dtpMiesiacPrzegladu.Value.Year.ToString() +
//"#, DATA_FAKTURY= #" + dtpDataFaktury.Value.Month.ToString() + "/" + dtpDataFaktury.Value.Day.ToString() + "/" + dtpDataFaktury.Value.Year.ToString() +
//"#, UWAGI='" + tboxUwagi.Text.Replace("'", "''") +
//"', NR_PROTOKOLU= '" + tboxNrProtokolu.Text.Replace("'", "''") +
//"', NR_FAKTURY='" + tboxNrFaktury.Text.Replace("'", "''") +
"#, ZROBIONY= " + checkBox1.Checked.ToString() +
", UWAGI='" + tboxUwagi.Text.Replace("'", "''") +"'"+
" WHERE ID=" + idPrzeglad + ";";
            }



            OleDbCommand SQLCommand = new OleDbCommand();
            SQLCommand.CommandText = SQLUpdateString;
            SQLCommand.Connection = f1.database;
            int response = -1;
            try
            {
                response = SQLCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            if (response >= 1) MessageBox.Show("Przegl�d zapisany do Bazy", "Sukces", MessageBoxButtons.OK, MessageBoxIcon.Information);
            {
                if (checkBox1.Checked)
                {
                    PrzegladFOrm ffa = new PrzegladFOrm();
                    ffa.TRYB = PrzegladFOrm.TrybENum.Dodawanie;
                    ffa.nowyPrzeglad1.DataPrzegladu = dtpDataPrzegladu.Value;
                    ffa.nowyPrzeglad1.IDFIRMY = idFirma;
                    ffa.nowyPrzeglad1.NazwaFirmy = nazwaFirmy;
                    ffa.nowyPrzeglad1.IDTypPrzegladu = idTypPrzegladu;
                    ffa.ShowDialog();
                }
                this.Parent.Dispose();
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            panel1.Visible = checkBox1.Checked;
        }
    }
}
