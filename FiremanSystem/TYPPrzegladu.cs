using System;
using System.Collections.Generic;
using System.Text;

namespace FiremanSystem
{
    public class TYPPrzegladu
    {
        private string id;

        public string ID
        {
            get { return id; }
            set { id = value; }
        }

        private string name;

        public string NAME
        {
            get { return name; }
            set { name = value; }
        }


        public override string ToString()
        {
            return NAME;
        }
	
    }
}
