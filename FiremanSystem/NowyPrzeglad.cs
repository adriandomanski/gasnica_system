using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace FiremanSystem
{
    public partial class NowyPrzeglad : UserControl
    {
        private int idFirmy;

        public int IDFIRMY
        {
            get { return idFirmy; }
            set { idFirmy = value; }
        }

        private string nazwaFirmy;

        public string NazwaFirmy
        {
            get { return nazwaFirmy; }
            set
            {
                nazwaFirmy = value;
                if (value != null)
                {
                    lblNazwaFirmy.Text = value;
                }
            }
        }


        private DateTime dataPrzegladu;

        public DateTime DataPrzegladu
        {
            get { return dataPrzegladu; }
            set
            {
                dataPrzegladu = value;
                dtpDataPrzegladu.Value = value;
            }
        }
	
        private int idTypPrzegladu;

        public int IDTypPrzegladu
        {
            get { return idTypPrzegladu; }
            set
            {
                idTypPrzegladu = value;

                switch (idTypPrzegladu)
                {
                    case 1:
                        dtpDataPrzegladu.Value = dtpDataPrzegladu.Value.AddDays(365);
                        break;
                    case 2:
                        dtpDataPrzegladu.Value = dtpDataPrzegladu.Value.AddDays(182);
                        break;
                    default:
                        break;
                }
            }
        }


        public NowyPrzeglad()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            FiremanSystemForm f1;


            string SQLString = "";

            f1 = new FiremanSystemForm();
        
            string data = dtpDataPrzegladu.Value.ToShortDateString();
            SQLString = "  INSERT INTO przeglad ( ID_FIRMA, Miesiac_przegladu, ZROBIONY, uwagi ) VALUES (" + idFirmy + ", #" + data + "#, false, '"+tboxUwagi.Text+"');";

                        OleDbCommand SQLCommand = new OleDbCommand();
            SQLCommand.CommandText = SQLString;
            SQLCommand.Connection = f1.database;
            int response = -1;
            try
            {
                response = SQLCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            if (response >= 1) MessageBox.Show("Przegl�d zapisany do Bazy", "Sukces", MessageBoxButtons.OK, MessageBoxIcon.Information);
            {
                this.Parent.Dispose();
            }
        }
    }
}