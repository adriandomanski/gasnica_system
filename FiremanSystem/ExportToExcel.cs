using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Forms;
using Microsoft.Office.Interop;
using Microsoft.VisualBasic;
using Microsoft.Office.Interop.Excel;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Configuration;
using Microsoft.VisualBasic;


namespace Tools.ExportToExcel
{
    public class ExportToExcel
    {
        public void dataGridView2Excel(DataGridView dataGridView, string pFullPath_toExport, string nameSheet)
        {
            Object obj = dataGridView.DataSource;
            System.Data.DataTable dt = new System.Data.DataTable();

            if (dataGridView.DataSource is DataSet)
            {
                if (((System.Data.DataSet)dataGridView.DataSource).Tables.Count > 0)
                    dt = ((System.Data.DataSet)dataGridView.DataSource).Tables[0];
                else
                    dt = new System.Data.DataTable();
            }
            else if (dataGridView.DataSource is System.Data.DataTable)
            {
                dt = (System.Data.DataTable)dataGridView.DataSource;
            }
            else if (dataGridView.DataSource is ArrayList)
            {
                ArrayList arr = (ArrayList)dataGridView.DataSource;
                dt = ArrayListToDataTable(arr);

            }


            dataTable2Excel(dt, dataGridView, pFullPath_toExport, nameSheet);
        }

        public void dataTable2Excel(System.Data.DataTable pDataTable, DataGridView dgv, string pFullPath_toExport, string nameSheet)
        {
            string vFileName = Path.GetTempFileName();
            FileSystem.FileOpen(1, vFileName, OpenMode.Output, OpenAccess.Default, OpenShare.Default, -1);

            string sb = string.Empty;
            if (dgv != null)
            {
                foreach (DataColumn dc in pDataTable.Columns)
                {
                    System.Windows.Forms.Application.DoEvents();
                    string title = string.Empty;

                    if (dgv.Columns[dc.Caption] != null)
                    {
                        title = dgv.Columns[dc.Caption].HeaderText;
                        sb += title + ControlChars.Tab;
                    }
                }
            }
            else
            {
                foreach (DataColumn dc in pDataTable.Columns)
                {
                    System.Windows.Forms.Application.DoEvents();
                    string title = string.Empty;

                    title = dc.Caption;
                    sb += title + ControlChars.Tab;

                }
            }

            FileSystem.PrintLine(1, sb);

            int i = 0;
            foreach (DataRow dr in pDataTable.Rows)
            {
                System.Windows.Forms.Application.DoEvents();
                i = 0;
                sb = string.Empty;
                foreach (DataColumn dc in pDataTable.Columns)
                {
                    if (dgv != null && dgv.Columns[dc.Caption] != null)
                    {
                        System.Windows.Forms.Application.DoEvents();
                        sb = sb + (Information.IsDBNull(dr[i]) ? string.Empty : FormatCell(dr[i])) + ControlChars.Tab;

                    }
                    else if (dgv == null)
                    {
                        System.Windows.Forms.Application.DoEvents();
                        sb = sb + (Information.IsDBNull(dr[i]) ? string.Empty : FormatCell(dr[i])) + ControlChars.Tab;
                    }
                    i++;
                }
                FileSystem.PrintLine(1, sb);
            }
            FileSystem.FileClose(1);
            TextToExcel(vFileName, pFullPath_toExport, nameSheet);
        }

        private string FormatCell(Object cell)
        {
            string TextToParse = Convert.ToString(cell);
            return TextToParse.Replace(",", string.Empty);
        }

        private void TextToExcel(string pFileName, string pFullPath_toExport, string nameSheet)
        {
            System.Globalization.CultureInfo vCultura = System.Threading.Thread.CurrentThread.CurrentCulture;
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("pl-PL");
            Microsoft.Office.Interop.Excel.Application Exc = new Microsoft.Office.Interop.Excel.Application();
            Exc.Workbooks.OpenText(pFileName, Missing.Value, 1,
                XlTextParsingType.xlDelimited,
                XlTextQualifier.xlTextQualifierNone,
                Missing.Value, Missing.Value,
                Missing.Value, true,
                Missing.Value, Missing.Value,
                Missing.Value, Missing.Value,
                Missing.Value, Missing.Value,
                Missing.Value, Missing.Value, Missing.Value);

            Workbook Wb = Exc.ActiveWorkbook;
            Worksheet Ws = (Worksheet)Wb.ActiveSheet;
            Ws.Name = nameSheet;

            try
            {
                Ws.get_Range(Ws.Cells[1, 1], Ws.Cells[Ws.UsedRange.Rows.Count, Ws.UsedRange.Columns.Count]).AutoFormat(XlRangeAutoFormat.xlRangeAutoFormatClassic1, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            }
            catch
            {
                Ws.get_Range(Ws.Cells[1, 1], Ws.Cells[Ws.UsedRange.Rows.Count, Ws.UsedRange.Columns.Count]);
            }

            string tempPath = Path.GetTempFileName();

            pFileName = tempPath.Replace("tmp", "xls");
            File.Delete(pFileName);

            if (File.Exists(pFullPath_toExport))
            {
                File.Delete(pFullPath_toExport);
            }
            Exc.ActiveWorkbook.SaveAs(pFullPath_toExport, 1, null, null, null, null, XlSaveAsAccessMode.xlNoChange, null, null, null, null, null);

            Exc.Workbooks.Close();

            System.Runtime.InteropServices.Marshal.ReleaseComObject(Ws);
            Ws = null;

            System.Runtime.InteropServices.Marshal.ReleaseComObject(Wb);
            Wb = null;

            Exc.Quit();

            System.Runtime.InteropServices.Marshal.ReleaseComObject(Exc);
            Exc = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            System.Threading.Thread.CurrentThread.CurrentCulture = vCultura;

        }

        public static System.Data.DataTable ArrayListToDataTable(ArrayList array)
        {
            System.Data.DataTable dt = new System.Data.DataTable();
            if (array.Count > 0)
            {
                object obj = array[0];
                foreach (PropertyInfo info in obj.GetType().GetProperties())
                {
                    dt.Columns.Add(info.Name, info.PropertyType);
                }
            }
            foreach (object obj in array)
            {
                DataRow dr = dt.NewRow();
                foreach (DataColumn col in dt.Columns)
                {
                    Type type = obj.GetType();

                    MemberInfo[] members = type.GetMember(col.ColumnName);

                    object valor;
                    if (members.Length != 0)
                    {
                        switch (members[0].MemberType)
                        {
                            case MemberTypes.Property:
                                PropertyInfo prop = (PropertyInfo)members[0];
                                try
                                {
                                    valor = prop.GetValue(obj, new object[0]);
                                }
                                catch
                                {
                                    valor = prop.GetValue(obj, null);
                                }

                                break;
                            case MemberTypes.Field:
                                FieldInfo field = (FieldInfo)members[0];
                                valor = field.GetValue(obj);
                                break;
                            default:
                                throw new NotImplementedException();
                        }
                        dr[col] = valor;
                    }
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }



        public static string readcell(Range oRange)
        {
            String result = string.Empty;
            if (oRange != null)
            {
                if (oRange.Text != null)
                {
                    result = oRange.Text.ToString();
                }
            }
            return result;
        }

    }
}
