namespace FiremanSystem
{
    partial class Kontakt
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tboxImie = new System.Windows.Forms.TextBox();
            this.tboxNazwisko = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tboxtel3 = new System.Windows.Forms.TextBox();
            this.tboxtel1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tboxtel2 = new System.Windows.Forms.TextBox();
            this.btnSave = new DevComponents.DotNetBar.ButtonX();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 18);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 13);
            this.label10.TabIndex = 47;
            this.label10.Text = "Imi�:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(9, 96);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 13);
            this.label13.TabIndex = 49;
            this.label13.Text = "Telefon 2:";
            // 
            // tboxImie
            // 
            this.tboxImie.Location = new System.Drawing.Point(3, 3);
            this.tboxImie.Name = "tboxImie";
            this.tboxImie.Size = new System.Drawing.Size(279, 20);
            this.tboxImie.TabIndex = 39;
            // 
            // tboxNazwisko
            // 
            this.tboxNazwisko.Location = new System.Drawing.Point(3, 29);
            this.tboxNazwisko.Name = "tboxNazwisko";
            this.tboxNazwisko.Size = new System.Drawing.Size(279, 20);
            this.tboxNazwisko.TabIndex = 40;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 70);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(55, 13);
            this.label12.TabIndex = 51;
            this.label12.Text = "Telefon 1:";
            // 
            // tboxtel3
            // 
            this.tboxtel3.Location = new System.Drawing.Point(3, 107);
            this.tboxtel3.Name = "tboxtel3";
            this.tboxtel3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tboxtel3.Size = new System.Drawing.Size(279, 20);
            this.tboxtel3.TabIndex = 43;
            // 
            // tboxtel1
            // 
            this.tboxtel1.Location = new System.Drawing.Point(3, 55);
            this.tboxtel1.Name = "tboxtel1";
            this.tboxtel1.Size = new System.Drawing.Size(279, 20);
            this.tboxtel1.TabIndex = 41;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 44);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 13);
            this.label11.TabIndex = 53;
            this.label11.Text = "Nazwisko:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(10, 122);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(108, 13);
            this.label14.TabIndex = 52;
            this.label14.Text = "Stanowisko/Funkcja:";
            // 
            // tboxtel2
            // 
            this.tboxtel2.Location = new System.Drawing.Point(3, 81);
            this.tboxtel2.Name = "tboxtel2";
            this.tboxtel2.Size = new System.Drawing.Size(279, 20);
            this.tboxtel2.TabIndex = 42;
            // 
            // btnSave
            // 
            this.btnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSave.Location = new System.Drawing.Point(331, 200);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 54;
            this.btnSave.Text = "Zapisz";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tboxImie);
            this.panel1.Controls.Add(this.tboxtel2);
            this.panel1.Controls.Add(this.tboxtel1);
            this.panel1.Controls.Add(this.tboxtel3);
            this.panel1.Controls.Add(this.tboxNazwisko);
            this.panel1.Location = new System.Drawing.Point(124, 11);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(287, 183);
            this.panel1.TabIndex = 55;
            // 
            // Kontakt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label14);
            this.Name = "Kontakt";
            this.Size = new System.Drawing.Size(421, 244);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tboxImie;
        private System.Windows.Forms.TextBox tboxNazwisko;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tboxtel3;
        private System.Windows.Forms.TextBox tboxtel1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tboxtel2;
        private DevComponents.DotNetBar.ButtonX btnSave;
        private System.Windows.Forms.Panel panel1;
    }
}
