using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace AppSpace
{
    public partial class Kontakt : UserControl
    {

        public enum TrybEnum
        {
            Nowy, Edycja
        }

        private TrybEnum tryb;

        public TrybEnum TRYB
        {
            get { return tryb; }
            set { tryb = value; }
        }
        private int idKontakt;

        public int IDKONTAKT
        {
            get { return idKontakt; }
            set { idKontakt = value; }
        }
        private int idFirma;

        public int IDFirma
        {
            get { return idFirma; }
            set { idFirma = value; }
        }
	

        public Kontakt()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Form1 f1;
            switch (tryb)
            {
                case TrybEnum.Nowy:
                    {
                        string SQLString = "";

                         f1 = new Form1();
                        //   SQLString = "INSERT INTO movie(Title, Publisher, Previewed, typeID) VALUES('" + name.Replace("'", "''") + "','" + publisher + "','" + previewed + "'," + type + ");"; //
                         SQLString = "INSERT INTO PRZEDSTAWICIEL(IMIE, NAZWISKO, TELEFON_1, TELEFON_2, TELEFON_3, ID_FIRMA) VALUES('"
                                    + tboxImie.Text + "','" + tboxNazwisko.Text + "','" + tboxtel1.Text + "','" + tboxtel2.Text + "','"
                                    + tboxtel3.Text + "',"+ IDFirma+ ");";


                        OleDbCommand SQLCommand = new OleDbCommand();
                        SQLCommand.CommandText = SQLString;
                        SQLCommand.Connection = f1.database;
                        int response = -1;
                        try
                        {
                            response = SQLCommand.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                        if (response >= 1) MessageBox.Show("Kontakt zapisany do Bazy", "Sukces", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        {
                           
                            this.Parent.Dispose();
                        }
                        

                    }
                    break;
                case TrybEnum.Edycja:
                    f1 = new Form1();
                    string SQLUpdateString;
                    SQLUpdateString = "UPDATE PRZEDSTAWICIEL SET IMIE ='" + tboxImie.Text.Replace("'", "''") +
                        "', NAZWISKO='" + tboxNazwisko.Text.Replace("'", "''") +
                        "', TELEFON_1='" + tboxtel1.Text.Replace("'", "''") +
                        "', TELEFON_2='" + tboxtel2.Text.Replace("'", "''") +
                        "', TELEFON_3='" + tboxtel3.Text.Replace("'", "''") +
                        "' WHERE ID=" + IDKONTAKT + ";";

                    OleDbCommand SQLCommand1 = new OleDbCommand();
                    SQLCommand1.CommandText = SQLUpdateString;
                    SQLCommand1.Connection = f1.database;
                    int response1 = SQLCommand1.ExecuteNonQuery();
                    if (response1 >= 1) MessageBox.Show("Kontakt zapisany do Bazy", "Sukces", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    {

                        this.Parent.Dispose();
                    }
                    break;
                default:
                    break;
            }

        }
        public void SetCOntrolByStrings(string imie, string nazwisko, string tel1, string tel2, string tel3, int idKontakt)
        {
            tboxImie.Text = imie;
            tboxNazwisko.Text = nazwisko;
            tboxtel1.Text = tel1;
            tboxtel2.Text = tel2;
            tboxtel3.Text = tel3;
            IDKONTAKT = idKontakt;
        }




    }
}
