using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace AppSpace
{
    public partial class NowyPrzeglad : UserControl
    {
        private int idFirmy;

        public int IDFIRMY
        {
            get { return idFirmy; }
            set { idFirmy = value; }
        }

        private string nazwaFirmy;

        public string NazwaFirmy
        {
            get { return nazwaFirmy; }
            set
            {
                nazwaFirmy = value;
                if (value != null)
                {
                    lblNazwaFirmy.Text = value;
                }
            }
        }


        private DateTime dataPrzegladu;

        public DateTime DataPrzegladu
        {
            get { return dataPrzegladu; }
            set
            {
                dataPrzegladu = value;
                dtpDataPrzegladu.Value = value;
            }
        }
	
        private int idTypPrzegladu;

        public int IDTypPrzegladu
        {
            get { return idTypPrzegladu; }
            set
            {
                idTypPrzegladu = value;

                switch (idTypPrzegladu)
                {
                    case 1:
                        dtpDataPrzegladu.Value = dtpDataPrzegladu.Value.AddDays(365);
                        break;
                    case 2:
                        dtpDataPrzegladu.Value = dtpDataPrzegladu.Value.AddDays(182);
                        break;
                    default:
                        break;
                }
            }
        }


        public NowyPrzeglad()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Form1 f1;


            string SQLString = "";

            f1 = new Form1();
            //   SQLString = "INSERT INTO movie(Title, Publisher, Previewed, typeID) VALUES('" + name.Replace("'", "''") + "','" + publisher + "','" + previewed + "'," + type + ");"; //
            //SQLString = "INSERT INTO PRZEDSTAWICIEL(IMIE, NAZWISKO, TELEFON_1, TELEFON_2, TELEFON_3, ID_FIRMA) VALUES('"
            //           + tboxImie.Text + "','" + tboxNazwisko.Text + "','" + tboxtel1.Text + "','" + tboxtel2.Text + "','"
            //           + tboxtel3.Text + "'," + IDFirma + ");";
            string data = dtpDataPrzegladu.Value.ToShortDateString();
            SQLString = "  INSERT INTO przeglad ( ID_FIRMA, Miesiac_przegladu, ZROBIONY, uwagi ) VALUES (" + idFirmy + ", #" + data + "#, false, '"+tboxUwagi.Text+"');";


            OleDbCommand SQLCommand = new OleDbCommand();
            SQLCommand.CommandText = SQLString;
            SQLCommand.Connection = f1.database;
            int response = -1;
            try
            {
                response = SQLCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            if (response >= 1) MessageBox.Show("Przegl�d zapisany do Bazy", "Sukces", MessageBoxButtons.OK, MessageBoxIcon.Information);
            {

                this.Parent.Dispose();
            }



        }
    }
}