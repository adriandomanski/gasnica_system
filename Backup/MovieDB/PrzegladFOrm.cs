using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AppSpace
{
    public partial class PrzegladFOrm : DevComponents.DotNetBar.Office2007Form
    {
        public enum TrybENum
        {
            Dodawanie, Edycja
        }
        private TrybENum tryb = TrybENum.Dodawanie;

        public TrybENum TRYB
        {
            get { return tryb; }
            set
            {
                przeglad1.Visible = false;
                nowyPrzeglad1.Visible = false;
                tryb = value;
                switch (tryb)
                {
                        
                    case TrybENum.Dodawanie:
                        nowyPrzeglad1.Visible = true;
                        this.Height = nowyPrzeglad1.Height + 40;
                        break;
                    case TrybENum.Edycja:

                        przeglad1.Visible = true;
                        this.Height = przeglad1.Height + 40;
                        break;
                    default:
                        break;
            }
            }
        }

        public PrzegladFOrm()
        {
            InitializeComponent();
        }
    }
}