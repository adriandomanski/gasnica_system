namespace AppSpace
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.button5 = new DevComponents.DotNetBar.ButtonX();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.button3 = new DevComponents.DotNetBar.ButtonX();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.button4 = new DevComponents.DotNetBar.ButtonX();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.button2 = new DevComponents.DotNetBar.ButtonX();
            this.button1 = new DevComponents.DotNetBar.ButtonX();
            this.label6 = new System.Windows.Forms.Label();
            this.tboxNazwaFirmy = new System.Windows.Forms.TextBox();
            this.combTyp = new System.Windows.Forms.ComboBox();
            this.button6 = new DevComponents.DotNetBar.ButtonX();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tboxNIP = new System.Windows.Forms.TextBox();
            this.tboxKodPocztowy = new System.Windows.Forms.TextBox();
            this.tboxNrDomu = new System.Windows.Forms.TextBox();
            this.tboxMiasto = new System.Windows.Forms.TextBox();
            this.tboxUlica = new System.Windows.Forms.TextBox();
            this.tboxNazwa = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.button7 = new DevComponents.DotNetBar.ButtonX();
            this.tabControl3 = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel2 = new DevComponents.DotNetBar.TabControlPanel();
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.tabItem2 = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel3 = new DevComponents.DotNetBar.TabControlPanel();
            this.dgvPrzeglady = new DevComponents.DotNetBar.Controls.DataGridViewX();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbWykonane = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.cbDoWykonania = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.cbWszystkie = new DevComponents.DotNetBar.Controls.CheckBoxX();
            this.lblPrzeglady = new DevComponents.DotNetBar.LabelX();
            this.dtpRok = new System.Windows.Forms.DateTimePicker();
            this.dtpMiesiac = new System.Windows.Forms.DateTimePicker();
            this.btnDrukuj = new DevComponents.DotNetBar.ButtonX();
            this.button9 = new DevComponents.DotNetBar.ButtonX();
            this.button8 = new DevComponents.DotNetBar.ButtonX();
            this.tabItem3 = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.groupPanel4 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tboxFakturaNazwa = new System.Windows.Forms.TextBox();
            this.tboxFakturaUlica = new System.Windows.Forms.TextBox();
            this.tboxFakturaMiasto = new System.Windows.Forms.TextBox();
            this.tboxFakturaKod = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.panelNewFirm = new System.Windows.Forms.Panel();
            this.tboxNrMieszkania = new System.Windows.Forms.TextBox();
            this.tboxFakturaNrMieszkania = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tboxFakturaNrDomu = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tabItem1 = new DevComponents.DotNetBar.TabItem(this.components);
            this.sfdExportToxcel = new System.Windows.Forms.SaveFileDialog();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl3)).BeginInit();
            this.tabControl3.SuspendLayout();
            this.tabControlPanel2.SuspendLayout();
            this.groupPanel1.SuspendLayout();
            this.tabControlPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrzeglady)).BeginInit();
            this.panel1.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            this.groupPanel4.SuspendLayout();
            this.groupPanel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            this.panelNewFirm.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Location = new System.Drawing.Point(361, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(869, 513);
            this.tabControl1.TabIndex = 1;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(861, 487);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Dodaj Firm�";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tabControl2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(861, 487);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Wyszukiwanie Firm";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Controls.Add(this.tabPage6);
            this.tabControl2.Location = new System.Drawing.Point(6, 6);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(849, 148);
            this.tabControl2.TabIndex = 0;
            this.tabControl2.Visible = false;
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(841, 122);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Search by title";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.button5);
            this.tabPage4.Controls.Add(this.comboBox2);
            this.tabPage4.Controls.Add(this.label7);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(841, 122);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "Search by type";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.button5.Location = new System.Drawing.Point(290, 27);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 2;
            this.button5.Text = "Search";
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Adventure",
            "Comedy",
            "Action",
            "Cartoon",
            "Romantic",
            "Fantasy",
            "Thriller",
            "Historic",
            "Drama",
            "Horor",
            "Sci-Fi",
            "Crime",
            "Biografy",
            "Documentary"});
            this.comboBox2.Location = new System.Drawing.Point(93, 29);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(179, 21);
            this.comboBox2.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(30, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Type: ";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.button3);
            this.tabPage5.Controls.Add(this.radioButton4);
            this.tabPage5.Controls.Add(this.radioButton3);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(841, 122);
            this.tabPage5.TabIndex = 2;
            this.tabPage5.Text = "Search by view";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.button3.Location = new System.Drawing.Point(242, 38);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Search";
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(132, 41);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(94, 17);
            this.radioButton4.TabIndex = 1;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Not previewed";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(41, 41);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(75, 17);
            this.radioButton3.TabIndex = 0;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Previewed";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.button4);
            this.tabPage6.Controls.Add(this.label9);
            this.tabPage6.Controls.Add(this.label8);
            this.tabPage6.Controls.Add(this.textBox6);
            this.tabPage6.Controls.Add(this.textBox5);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(841, 122);
            this.tabPage6.TabIndex = 3;
            this.tabPage6.Text = "Search by year";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.button4.Location = new System.Drawing.Point(386, 46);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 4;
            this.button4.Text = "Search";
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 51);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Find movie between:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(241, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "and";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(270, 48);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(100, 20);
            this.textBox6.TabIndex = 1;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(137, 48);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(100, 20);
            this.textBox5.TabIndex = 0;
            // 
            // tabPage7
            // 
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(861, 487);
            this.tabPage7.TabIndex = 2;
            this.tabPage7.Text = "Przegl�dy";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.button2.Location = new System.Drawing.Point(473, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(92, 20);
            this.button2.TabIndex = 4;
            this.button2.Text = "Wszystkie dane";
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.button1.Location = new System.Drawing.Point(392, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 20);
            this.button1.TabIndex = 3;
            this.button1.Text = "Szukaj";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(2, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Nazwa: ";
            // 
            // tboxNazwaFirmy
            // 
            this.tboxNazwaFirmy.Location = new System.Drawing.Point(54, 3);
            this.tboxNazwaFirmy.Name = "tboxNazwaFirmy";
            this.tboxNazwaFirmy.Size = new System.Drawing.Size(332, 20);
            this.tboxNazwaFirmy.TabIndex = 1;
            this.tboxNazwaFirmy.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tboxNazwaFirmy_KeyDown);
            // 
            // combTyp
            // 
            this.combTyp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combTyp.FormattingEnabled = true;
            this.combTyp.Location = new System.Drawing.Point(3, 104);
            this.combTyp.Name = "combTyp";
            this.combTyp.Size = new System.Drawing.Size(290, 21);
            this.combTyp.TabIndex = 6;
            this.combTyp.Tag = "Wybierz typ przegl�du";
            this.combTyp.SelectedIndexChanged += new System.EventHandler(this.combTyp_SelectedIndexChanged);
            // 
            // button6
            // 
            this.button6.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.button6.Location = new System.Drawing.Point(797, 167);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(103, 21);
            this.button6.TabIndex = 0;
            this.button6.Text = "Zapisz";
            this.button6.Click += new System.EventHandler(this.button6_Click);
            this.button6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.button6_KeyDown);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Location = new System.Drawing.Point(3, 112);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(78, 13);
            this.label17.TabIndex = 6;
            this.label17.Text = "Typ Przegl�du:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Location = new System.Drawing.Point(3, 109);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(28, 13);
            this.label16.TabIndex = 6;
            this.label16.Text = "NIP:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Location = new System.Drawing.Point(22, 296);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "Numer Domu:";
            this.label12.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Location = new System.Drawing.Point(3, 86);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(78, 13);
            this.label15.TabIndex = 6;
            this.label15.Text = "Kod Pocztowy:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(3, 31);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "Adres:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Location = new System.Drawing.Point(3, 60);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "Miasto:";
            // 
            // tboxNIP
            // 
            this.tboxNIP.Location = new System.Drawing.Point(3, 104);
            this.tboxNIP.Name = "tboxNIP";
            this.tboxNIP.Size = new System.Drawing.Size(290, 20);
            this.tboxNIP.TabIndex = 6;
            this.tboxNIP.Tag = "Wpisz NIP\n";
            this.tboxNIP.TextChanged += new System.EventHandler(this.tboxNIP_TextChanged);
            // 
            // tboxKodPocztowy
            // 
            this.tboxKodPocztowy.Location = new System.Drawing.Point(3, 79);
            this.tboxKodPocztowy.MaxLength = 20;
            this.tboxKodPocztowy.Name = "tboxKodPocztowy";
            this.tboxKodPocztowy.Size = new System.Drawing.Size(290, 20);
            this.tboxKodPocztowy.TabIndex = 5;
            this.tboxKodPocztowy.Tag = "Wpisz kod pocztowy\n";
            this.tboxKodPocztowy.TextChanged += new System.EventHandler(this.tboxKodPocztowy_TextChanged);
            // 
            // tboxNrDomu
            // 
            this.tboxNrDomu.Location = new System.Drawing.Point(145, 287);
            this.tboxNrDomu.Name = "tboxNrDomu";
            this.tboxNrDomu.Size = new System.Drawing.Size(290, 20);
            this.tboxNrDomu.TabIndex = 2;
            this.tboxNrDomu.Tag = "Wpisz Numer domu\n";
            this.tboxNrDomu.Visible = false;
            this.tboxNrDomu.TextChanged += new System.EventHandler(this.tboxNrDomu_TextChanged);
            // 
            // tboxMiasto
            // 
            this.tboxMiasto.Location = new System.Drawing.Point(3, 53);
            this.tboxMiasto.MaxLength = 200;
            this.tboxMiasto.Name = "tboxMiasto";
            this.tboxMiasto.Size = new System.Drawing.Size(290, 20);
            this.tboxMiasto.TabIndex = 4;
            this.tboxMiasto.Tag = "Wpisz Miasto\n";
            this.tboxMiasto.TextChanged += new System.EventHandler(this.tboxMiasto_TextChanged);
            // 
            // tboxUlica
            // 
            this.tboxUlica.Location = new System.Drawing.Point(3, 27);
            this.tboxUlica.MaxLength = 200;
            this.tboxUlica.Name = "tboxUlica";
            this.tboxUlica.Size = new System.Drawing.Size(290, 20);
            this.tboxUlica.TabIndex = 1;
            this.tboxUlica.Tag = "Wpisz Ulic�\n";
            this.tboxUlica.TextChanged += new System.EventHandler(this.tboxUlica_TextChanged);
            // 
            // tboxNazwa
            // 
            this.tboxNazwa.Location = new System.Drawing.Point(3, 1);
            this.tboxNazwa.MaxLength = 200;
            this.tboxNazwa.Name = "tboxNazwa";
            this.tboxNazwa.Size = new System.Drawing.Size(290, 20);
            this.tboxNazwa.TabIndex = 0;
            this.tboxNazwa.Tag = "Wpisz Nazw�\n";
            this.tboxNazwa.TextChanged += new System.EventHandler(this.tboxNazwa_TextChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(217)))), ((int)(((byte)(247)))));
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dataGridView1.Location = new System.Drawing.Point(1, 33);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(1014, 606);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            this.dataGridView1.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_ColumnHeaderMouseClick);
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // button7
            // 
            this.button7.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.button7.Location = new System.Drawing.Point(0, 68);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(891, 23);
            this.button7.TabIndex = 2;
            this.button7.Text = "Od�wie� dane";
            this.button7.Visible = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // tabControl3
            // 
            this.tabControl3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(217)))), ((int)(((byte)(247)))));
            this.tabControl3.CanReorderTabs = false;
            this.tabControl3.Controls.Add(this.tabControlPanel3);
            this.tabControl3.Controls.Add(this.tabControlPanel1);
            this.tabControl3.Controls.Add(this.tabControlPanel2);
            this.tabControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl3.Location = new System.Drawing.Point(0, 0);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tabControl3.SelectedTabIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(1016, 666);
            this.tabControl3.TabIndex = 2;
            this.tabControl3.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tabControl3.Tabs.Add(this.tabItem2);
            this.tabControl3.Tabs.Add(this.tabItem3);
            this.tabControl3.Tabs.Add(this.tabItem1);
            this.tabControl3.Text = "tabControl3";
            // 
            // tabControlPanel2
            // 
            this.tabControlPanel2.Controls.Add(this.dataGridView1);
            this.tabControlPanel2.Controls.Add(this.groupPanel1);
            this.tabControlPanel2.Controls.Add(this.button7);
            this.tabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel2.Location = new System.Drawing.Point(0, 26);
            this.tabControlPanel2.Name = "tabControlPanel2";
            this.tabControlPanel2.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel2.Size = new System.Drawing.Size(1016, 640);
            this.tabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(179)))), ((int)(((byte)(231)))));
            this.tabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.tabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.tabControlPanel2.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel2.Style.GradientAngle = 90;
            this.tabControlPanel2.TabIndex = 2;
            this.tabControlPanel2.TabItem = this.tabItem2;
            // 
            // groupPanel1
            // 
            this.groupPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(217)))), ((int)(((byte)(247)))));
            this.groupPanel1.CanvasColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(217)))), ((int)(((byte)(247)))));
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.buttonX1);
            this.groupPanel1.Controls.Add(this.button2);
            this.groupPanel1.Controls.Add(this.tboxNazwaFirmy);
            this.groupPanel1.Controls.Add(this.button1);
            this.groupPanel1.Controls.Add(this.label6);
            this.groupPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupPanel1.Location = new System.Drawing.Point(1, 1);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(1014, 32);
            // 
            // 
            // 
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            this.groupPanel1.TabIndex = 3;
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.Location = new System.Drawing.Point(581, 3);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Size = new System.Drawing.Size(92, 20);
            this.buttonX1.TabIndex = 4;
            this.buttonX1.Text = "Wszystkie dane";
            // 
            // tabItem2
            // 
            this.tabItem2.AttachedControl = this.tabControlPanel2;
            this.tabItem2.Name = "tabItem2";
            this.tabItem2.Text = "Spis Firm";
            this.tabItem2.Click += new System.EventHandler(this.tabItem2_Click);
            // 
            // tabControlPanel3
            // 
            this.tabControlPanel3.Controls.Add(this.dgvPrzeglady);
            this.tabControlPanel3.Controls.Add(this.panel1);
            this.tabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel3.Location = new System.Drawing.Point(0, 26);
            this.tabControlPanel3.Margin = new System.Windows.Forms.Padding(3, 35, 3, 3);
            this.tabControlPanel3.Name = "tabControlPanel3";
            this.tabControlPanel3.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel3.Size = new System.Drawing.Size(1016, 640);
            this.tabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(179)))), ((int)(((byte)(231)))));
            this.tabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.tabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel3.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.tabControlPanel3.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel3.Style.GradientAngle = 90;
            this.tabControlPanel3.TabIndex = 3;
            this.tabControlPanel3.TabItem = this.tabItem3;
            // 
            // dgvPrzeglady
            // 
            this.dgvPrzeglady.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(217)))), ((int)(((byte)(247)))));
            this.dgvPrzeglady.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPrzeglady.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPrzeglady.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPrzeglady.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPrzeglady.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgvPrzeglady.Location = new System.Drawing.Point(1, 80);
            this.dgvPrzeglady.Margin = new System.Windows.Forms.Padding(3, 35, 3, 3);
            this.dgvPrzeglady.MultiSelect = false;
            this.dgvPrzeglady.Name = "dgvPrzeglady";
            this.dgvPrzeglady.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvPrzeglady.Size = new System.Drawing.Size(1014, 559);
            this.dgvPrzeglady.TabIndex = 2;
            this.dgvPrzeglady.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvPrzeglady_KeyDown);
            this.dgvPrzeglady.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPrzeglady_CellClick);
            this.dgvPrzeglady.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPrzeglady_CellDoubleClick);
            this.dgvPrzeglady.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPrzeglady_ColumnHeaderMouseClick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(217)))), ((int)(((byte)(247)))));
            this.panel1.Controls.Add(this.cbWykonane);
            this.panel1.Controls.Add(this.cbDoWykonania);
            this.panel1.Controls.Add(this.cbWszystkie);
            this.panel1.Controls.Add(this.lblPrzeglady);
            this.panel1.Controls.Add(this.dtpRok);
            this.panel1.Controls.Add(this.dtpMiesiac);
            this.panel1.Controls.Add(this.btnDrukuj);
            this.panel1.Controls.Add(this.button9);
            this.panel1.Controls.Add(this.button8);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1014, 79);
            this.panel1.TabIndex = 6;
            // 
            // cbWykonane
            // 
            this.cbWykonane.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.cbWykonane.Location = new System.Drawing.Point(350, 3);
            this.cbWykonane.Name = "cbWykonane";
            this.cbWykonane.Size = new System.Drawing.Size(128, 23);
            this.cbWykonane.TabIndex = 7;
            this.cbWykonane.Text = "Przegl�dy wykonane";
            // 
            // cbDoWykonania
            // 
            this.cbDoWykonania.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.cbDoWykonania.Checked = true;
            this.cbDoWykonania.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbDoWykonania.CheckValue = "Y";
            this.cbDoWykonania.Location = new System.Drawing.Point(198, 3);
            this.cbDoWykonania.Name = "cbDoWykonania";
            this.cbDoWykonania.Size = new System.Drawing.Size(146, 23);
            this.cbDoWykonania.TabIndex = 7;
            this.cbDoWykonania.Text = "Przegl�dy do wykonania";
            // 
            // cbWszystkie
            // 
            this.cbWszystkie.CheckBoxStyle = DevComponents.DotNetBar.eCheckBoxStyle.RadioButton;
            this.cbWszystkie.Location = new System.Drawing.Point(484, 3);
            this.cbWszystkie.Name = "cbWszystkie";
            this.cbWszystkie.Size = new System.Drawing.Size(125, 23);
            this.cbWszystkie.TabIndex = 7;
            this.cbWszystkie.Text = "Wszystkie przegl�dy";
            // 
            // lblPrzeglady
            // 
            this.lblPrzeglady.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblPrzeglady.Location = new System.Drawing.Point(4, 56);
            this.lblPrzeglady.Name = "lblPrzeglady";
            this.lblPrzeglady.Size = new System.Drawing.Size(474, 23);
            this.lblPrzeglady.TabIndex = 6;
            // 
            // dtpRok
            // 
            this.dtpRok.CustomFormat = "yyyy";
            this.dtpRok.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpRok.Location = new System.Drawing.Point(3, 3);
            this.dtpRok.Name = "dtpRok";
            this.dtpRok.ShowUpDown = true;
            this.dtpRok.Size = new System.Drawing.Size(83, 20);
            this.dtpRok.TabIndex = 5;
            // 
            // dtpMiesiac
            // 
            this.dtpMiesiac.CustomFormat = "MMMMMMMMMMMM";
            this.dtpMiesiac.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMiesiac.Location = new System.Drawing.Point(92, 3);
            this.dtpMiesiac.Name = "dtpMiesiac";
            this.dtpMiesiac.ShowUpDown = true;
            this.dtpMiesiac.Size = new System.Drawing.Size(100, 20);
            this.dtpMiesiac.TabIndex = 5;
            this.dtpMiesiac.ValueChanged += new System.EventHandler(this.dateTimePicker2_ValueChanged);
            // 
            // btnDrukuj
            // 
            this.btnDrukuj.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDrukuj.Location = new System.Drawing.Point(3, 32);
            this.btnDrukuj.Name = "btnDrukuj";
            this.btnDrukuj.Size = new System.Drawing.Size(93, 20);
            this.btnDrukuj.TabIndex = 4;
            this.btnDrukuj.Text = "Drukuj";
            this.btnDrukuj.Click += new System.EventHandler(this.btnDrukuj_Click);
            // 
            // button9
            // 
            this.button9.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.button9.Location = new System.Drawing.Point(399, 32);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(93, 20);
            this.button9.TabIndex = 4;
            this.button9.Text = "Poka� wszystkie";
            this.button9.Tooltip = "Pokazuje wszystke przegl�dy  wykonane i niewykonane dla wysztstkich firm bez ogra" +
                "nicze� czasowych";
            this.button9.Visible = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.button8.Location = new System.Drawing.Point(534, 32);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 20);
            this.button8.TabIndex = 4;
            this.button8.Text = "Szukaj";
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // tabItem3
            // 
            this.tabItem3.AttachedControl = this.tabControlPanel3;
            this.tabItem3.Name = "tabItem3";
            this.tabItem3.Text = "Przegl�dy";
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.Controls.Add(this.groupPanel4);
            this.tabControlPanel1.Controls.Add(this.tboxNrMieszkania);
            this.tabControlPanel1.Controls.Add(this.tboxFakturaNrMieszkania);
            this.tabControlPanel1.Controls.Add(this.tboxNrDomu);
            this.tabControlPanel1.Controls.Add(this.label1);
            this.tabControlPanel1.Controls.Add(this.tboxFakturaNrDomu);
            this.tabControlPanel1.Controls.Add(this.label12);
            this.tabControlPanel1.Controls.Add(this.label10);
            this.tabControlPanel1.Controls.Add(this.label18);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 26);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(1016, 640);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(179)))), ((int)(((byte)(231)))));
            this.tabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 1;
            this.tabControlPanel1.TabItem = this.tabItem1;
            // 
            // groupPanel4
            // 
            this.groupPanel4.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel4.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel4.Controls.Add(this.button6);
            this.groupPanel4.Controls.Add(this.groupPanel3);
            this.groupPanel4.Controls.Add(this.groupPanel2);
            this.groupPanel4.Location = new System.Drawing.Point(4, 11);
            this.groupPanel4.Name = "groupPanel4";
            this.groupPanel4.Size = new System.Drawing.Size(912, 207);
            // 
            // 
            // 
            this.groupPanel4.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.groupPanel4.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderBottomWidth = 1;
            this.groupPanel4.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel4.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderLeftWidth = 1;
            this.groupPanel4.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderRightWidth = 1;
            this.groupPanel4.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderTopWidth = 1;
            this.groupPanel4.Style.CornerDiameter = 4;
            this.groupPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel4.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel4.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel4.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            this.groupPanel4.TabIndex = 16;
            // 
            // groupPanel3
            // 
            this.groupPanel3.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel3.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.Controls.Add(this.label4);
            this.groupPanel3.Controls.Add(this.label3);
            this.groupPanel3.Controls.Add(this.label5);
            this.groupPanel3.Controls.Add(this.panel2);
            this.groupPanel3.Controls.Add(this.label13);
            this.groupPanel3.Controls.Add(this.label16);
            this.groupPanel3.Location = new System.Drawing.Point(458, 3);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.Size = new System.Drawing.Size(442, 158);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            this.groupPanel3.TabIndex = 16;
            this.groupPanel3.Text = "Faktura";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(3, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Nazwa:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(3, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Miasto:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(3, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Adres:";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.tboxFakturaNazwa);
            this.panel2.Controls.Add(this.tboxFakturaUlica);
            this.panel2.Controls.Add(this.tboxFakturaMiasto);
            this.panel2.Controls.Add(this.tboxFakturaKod);
            this.panel2.Controls.Add(this.tboxNIP);
            this.panel2.Location = new System.Drawing.Point(136, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(298, 192);
            this.panel2.TabIndex = 15;
            // 
            // tboxFakturaNazwa
            // 
            this.tboxFakturaNazwa.Location = new System.Drawing.Point(3, 1);
            this.tboxFakturaNazwa.MaxLength = 200;
            this.tboxFakturaNazwa.Name = "tboxFakturaNazwa";
            this.tboxFakturaNazwa.Size = new System.Drawing.Size(290, 20);
            this.tboxFakturaNazwa.TabIndex = 0;
            this.tboxFakturaNazwa.Tag = "Wpisz Nazw�do faktury\n";
            // 
            // tboxFakturaUlica
            // 
            this.tboxFakturaUlica.Location = new System.Drawing.Point(3, 27);
            this.tboxFakturaUlica.MaxLength = 200;
            this.tboxFakturaUlica.Name = "tboxFakturaUlica";
            this.tboxFakturaUlica.Size = new System.Drawing.Size(290, 20);
            this.tboxFakturaUlica.TabIndex = 1;
            this.tboxFakturaUlica.Tag = "Wpisz Ulic�\n";
            // 
            // tboxFakturaMiasto
            // 
            this.tboxFakturaMiasto.Location = new System.Drawing.Point(3, 53);
            this.tboxFakturaMiasto.MaxLength = 200;
            this.tboxFakturaMiasto.Name = "tboxFakturaMiasto";
            this.tboxFakturaMiasto.Size = new System.Drawing.Size(290, 20);
            this.tboxFakturaMiasto.TabIndex = 4;
            this.tboxFakturaMiasto.Tag = "Wpisz Miasto\n";
            // 
            // tboxFakturaKod
            // 
            this.tboxFakturaKod.Location = new System.Drawing.Point(3, 79);
            this.tboxFakturaKod.MaxLength = 20;
            this.tboxFakturaKod.Name = "tboxFakturaKod";
            this.tboxFakturaKod.Size = new System.Drawing.Size(290, 20);
            this.tboxFakturaKod.TabIndex = 5;
            this.tboxFakturaKod.Tag = "";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Location = new System.Drawing.Point(3, 84);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(78, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "Kod Pocztowy:";
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel2.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.label2);
            this.groupPanel2.Controls.Add(this.label14);
            this.groupPanel2.Controls.Add(this.label11);
            this.groupPanel2.Controls.Add(this.panelNewFirm);
            this.groupPanel2.Controls.Add(this.label15);
            this.groupPanel2.Controls.Add(this.label17);
            this.groupPanel2.Location = new System.Drawing.Point(3, 3);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(449, 158);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            this.groupPanel2.TabIndex = 16;
            this.groupPanel2.Text = "Protok�";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(3, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Nazwa:";
            // 
            // panelNewFirm
            // 
            this.panelNewFirm.BackColor = System.Drawing.Color.Transparent;
            this.panelNewFirm.Controls.Add(this.tboxNazwa);
            this.panelNewFirm.Controls.Add(this.tboxUlica);
            this.panelNewFirm.Controls.Add(this.tboxMiasto);
            this.panelNewFirm.Controls.Add(this.tboxKodPocztowy);
            this.panelNewFirm.Controls.Add(this.combTyp);
            this.panelNewFirm.Location = new System.Drawing.Point(142, 2);
            this.panelNewFirm.Name = "panelNewFirm";
            this.panelNewFirm.Size = new System.Drawing.Size(296, 191);
            this.panelNewFirm.TabIndex = 14;
            // 
            // tboxNrMieszkania
            // 
            this.tboxNrMieszkania.Location = new System.Drawing.Point(145, 313);
            this.tboxNrMieszkania.Name = "tboxNrMieszkania";
            this.tboxNrMieszkania.Size = new System.Drawing.Size(290, 20);
            this.tboxNrMieszkania.TabIndex = 3;
            this.tboxNrMieszkania.Tag = "Wpisz Numer mieszkania\n";
            this.tboxNrMieszkania.Visible = false;
            this.tboxNrMieszkania.TextChanged += new System.EventHandler(this.tboxNrDomu_TextChanged);
            // 
            // tboxFakturaNrMieszkania
            // 
            this.tboxFakturaNrMieszkania.Location = new System.Drawing.Point(585, 289);
            this.tboxFakturaNrMieszkania.Name = "tboxFakturaNrMieszkania";
            this.tboxFakturaNrMieszkania.Size = new System.Drawing.Size(290, 20);
            this.tboxFakturaNrMieszkania.TabIndex = 3;
            this.tboxFakturaNrMieszkania.Tag = "Wpisz Numer domu\n";
            this.tboxFakturaNrMieszkania.Visible = false;
            this.tboxFakturaNrMieszkania.TextChanged += new System.EventHandler(this.tboxFakturaNrDomu_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(22, 323);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Numer Mieszkania:";
            this.label1.Visible = false;
            // 
            // tboxFakturaNrDomu
            // 
            this.tboxFakturaNrDomu.Location = new System.Drawing.Point(585, 322);
            this.tboxFakturaNrDomu.Name = "tboxFakturaNrDomu";
            this.tboxFakturaNrDomu.Size = new System.Drawing.Size(290, 20);
            this.tboxFakturaNrDomu.TabIndex = 2;
            this.tboxFakturaNrDomu.Tag = "Wpisz Numer domu\n";
            this.tboxFakturaNrDomu.Visible = false;
            this.tboxFakturaNrDomu.TextChanged += new System.EventHandler(this.tboxFakturaNrDomu_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(475, 287);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(97, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Numer Mieszkania:";
            this.label10.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Location = new System.Drawing.Point(475, 313);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(72, 13);
            this.label18.TabIndex = 6;
            this.label18.Text = "Numer Domu:";
            this.label18.Visible = false;
            // 
            // tabItem1
            // 
            this.tabItem1.AttachedControl = this.tabControlPanel1;
            this.tabItem1.Name = "tabItem1";
            this.tabItem1.Text = "Dodaj Firm�";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1016, 666);
            this.Controls.Add(this.tabControl3);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Spis wszystkich firm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl3)).EndInit();
            this.tabControl3.ResumeLayout(false);
            this.tabControlPanel2.ResumeLayout(false);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel1.PerformLayout();
            this.tabControlPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPrzeglady)).EndInit();
            this.panel1.ResumeLayout(false);
            this.tabControlPanel1.ResumeLayout(false);
            this.tabControlPanel1.PerformLayout();
            this.groupPanel4.ResumeLayout(false);
            this.groupPanel3.ResumeLayout(false);
            this.groupPanel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupPanel2.ResumeLayout(false);
            this.groupPanel2.PerformLayout();
            this.panelNewFirm.ResumeLayout(false);
            this.panelNewFirm.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage3;
        private DevComponents.DotNetBar.ButtonX button2;
        private DevComponents.DotNetBar.ButtonX button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tboxNazwaFirmy;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private DevComponents.DotNetBar.ButtonX button5;
        private DevComponents.DotNetBar.ButtonX button6;
        public System.Windows.Forms.TabPage tabPage6;
        private DevComponents.DotNetBar.ButtonX button3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private DevComponents.DotNetBar.ButtonX button4;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TextBox tboxNrDomu;
        private System.Windows.Forms.TextBox tboxUlica;
        private System.Windows.Forms.TextBox tboxNazwa;
        private System.Windows.Forms.TextBox tboxNIP;
        private System.Windows.Forms.TextBox tboxKodPocztowy;
        private System.Windows.Forms.TextBox tboxMiasto;
        private System.Windows.Forms.ComboBox combTyp;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label14;
        private DevComponents.DotNetBar.ButtonX button7;
        private DevComponents.DotNetBar.TabControl tabControl3;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel1;
        private DevComponents.DotNetBar.TabItem tabItem1;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel2;
        private DevComponents.DotNetBar.TabItem tabItem2;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel3;
        private DevComponents.DotNetBar.TabItem tabItem3;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.ButtonX button8;
        private System.Windows.Forms.DateTimePicker dtpMiesiac;
        private System.Windows.Forms.DateTimePicker dtpRok;
        private System.Windows.Forms.Panel panel1;
        private DevComponents.DotNetBar.ButtonX button9;
        private DevComponents.DotNetBar.ButtonX btnDrukuj;
        private System.Windows.Forms.SaveFileDialog sfdExportToxcel;
        private DevComponents.DotNetBar.Controls.DataGridViewX dataGridView1;
        private DevComponents.DotNetBar.Controls.DataGridViewX dgvPrzeglady;
        private System.Windows.Forms.Panel panelNewFirm;
        private DevComponents.DotNetBar.LabelX lblPrzeglady;
        private DevComponents.DotNetBar.Controls.CheckBoxX cbWykonane;
        private DevComponents.DotNetBar.Controls.CheckBoxX cbDoWykonania;
        private DevComponents.DotNetBar.Controls.CheckBoxX cbWszystkie;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox tboxFakturaNazwa;
        private System.Windows.Forms.TextBox tboxFakturaUlica;
        private System.Windows.Forms.TextBox tboxFakturaNrDomu;
        private System.Windows.Forms.TextBox tboxFakturaMiasto;
        private System.Windows.Forms.TextBox tboxFakturaKod;
        private System.Windows.Forms.Label label2;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel4;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label18;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private System.Windows.Forms.TextBox tboxFakturaNrMieszkania;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tboxNrMieszkania;
        private System.Windows.Forms.Label label1;
        private DevComponents.DotNetBar.ButtonX buttonX1;
    }
}

