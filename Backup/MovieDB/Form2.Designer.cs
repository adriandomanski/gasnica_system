namespace AppSpace
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.combTyp = new System.Windows.Forms.ComboBox();
            this.tboxNIP = new System.Windows.Forms.TextBox();
            this.tboxKodPocztowy = new System.Windows.Forms.TextBox();
            this.tboxNrDomu = new System.Windows.Forms.TextBox();
            this.tboxMiasto = new System.Windows.Forms.TextBox();
            this.tboxUlica = new System.Windows.Forms.TextBox();
            this.tboxNazwa = new System.Windows.Forms.TextBox();
            this.button6 = new DevComponents.DotNetBar.ButtonX();
            this.dgKontakty = new System.Windows.Forms.DataGridView();
            this.btnAddKontakt = new DevComponents.DotNetBar.ButtonX();
            this.tabControl1 = new DevComponents.DotNetBar.TabControl();
            this.tabControlPanel2 = new DevComponents.DotNetBar.TabControlPanel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.dgPrzeglady = new System.Windows.Forms.DataGridView();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnDodajPrzeglad = new DevComponents.DotNetBar.ButtonX();
            this.tabItem2 = new DevComponents.DotNetBar.TabItem(this.components);
            this.tabControlPanel1 = new DevComponents.DotNetBar.TabControlPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabItem1 = new DevComponents.DotNetBar.TabItem(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupPanel4 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.groupPanel3 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tboxFakturaNazwa = new System.Windows.Forms.TextBox();
            this.tboxFakturaUlica = new System.Windows.Forms.TextBox();
            this.tboxFakturaMiasto = new System.Windows.Forms.TextBox();
            this.tboxFakturaKod = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panelNewFirm = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.tboxFakturaNrMieszkania = new System.Windows.Forms.TextBox();
            this.tboxFakturaNrDomu = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tboxnrMieszkania = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgKontakty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabControlPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgPrzeglady)).BeginInit();
            this.panel3.SuspendLayout();
            this.tabControlPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupPanel4.SuspendLayout();
            this.groupPanel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            this.panelNewFirm.SuspendLayout();
            this.SuspendLayout();
            // 
            // combTyp
            // 
            this.combTyp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combTyp.FormattingEnabled = true;
            this.combTyp.Location = new System.Drawing.Point(3, 102);
            this.combTyp.Name = "combTyp";
            this.combTyp.Size = new System.Drawing.Size(290, 21);
            this.combTyp.TabIndex = 6;
            // 
            // tboxNIP
            // 
            this.tboxNIP.Location = new System.Drawing.Point(3, 105);
            this.tboxNIP.MaxLength = 20;
            this.tboxNIP.Name = "tboxNIP";
            this.tboxNIP.Size = new System.Drawing.Size(290, 20);
            this.tboxNIP.TabIndex = 6;
            // 
            // tboxKodPocztowy
            // 
            this.tboxKodPocztowy.Location = new System.Drawing.Point(3, 79);
            this.tboxKodPocztowy.MaxLength = 20;
            this.tboxKodPocztowy.Name = "tboxKodPocztowy";
            this.tboxKodPocztowy.Size = new System.Drawing.Size(290, 20);
            this.tboxKodPocztowy.TabIndex = 5;
            // 
            // tboxNrDomu
            // 
            this.tboxNrDomu.Location = new System.Drawing.Point(130, 166);
            this.tboxNrDomu.Name = "tboxNrDomu";
            this.tboxNrDomu.Size = new System.Drawing.Size(290, 20);
            this.tboxNrDomu.TabIndex = 2;
            this.tboxNrDomu.Visible = false;
            // 
            // tboxMiasto
            // 
            this.tboxMiasto.Location = new System.Drawing.Point(3, 53);
            this.tboxMiasto.MaxLength = 200;
            this.tboxMiasto.Name = "tboxMiasto";
            this.tboxMiasto.Size = new System.Drawing.Size(290, 20);
            this.tboxMiasto.TabIndex = 4;
            // 
            // tboxUlica
            // 
            this.tboxUlica.Location = new System.Drawing.Point(3, 29);
            this.tboxUlica.MaxLength = 200;
            this.tboxUlica.Name = "tboxUlica";
            this.tboxUlica.Size = new System.Drawing.Size(290, 20);
            this.tboxUlica.TabIndex = 1;
            // 
            // tboxNazwa
            // 
            this.tboxNazwa.Location = new System.Drawing.Point(3, 3);
            this.tboxNazwa.MaxLength = 200;
            this.tboxNazwa.Name = "tboxNazwa";
            this.tboxNazwa.Size = new System.Drawing.Size(290, 20);
            this.tboxNazwa.TabIndex = 0;
            // 
            // button6
            // 
            this.button6.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.button6.Location = new System.Drawing.Point(784, 169);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(103, 25);
            this.button6.TabIndex = 0;
            this.button6.Text = "Zapisz";
            this.button6.Click += new System.EventHandler(this.button6_Click);
            this.button6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.button6_KeyDown);
            // 
            // dgKontakty
            // 
            this.dgKontakty.AllowUserToDeleteRows = false;
            this.dgKontakty.AllowUserToResizeRows = false;
            this.dgKontakty.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(217)))), ((int)(((byte)(247)))));
            this.dgKontakty.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgKontakty.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgKontakty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgKontakty.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgKontakty.Location = new System.Drawing.Point(1, 29);
            this.dgKontakty.MultiSelect = false;
            this.dgKontakty.Name = "dgKontakty";
            this.dgKontakty.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgKontakty.Size = new System.Drawing.Size(903, 350);
            this.dgKontakty.TabIndex = 3;
            this.dgKontakty.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgKontakty_CellDoubleClick);
            this.dgKontakty.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgKontakty_CellContentClick);
            // 
            // btnAddKontakt
            // 
            this.btnAddKontakt.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnAddKontakt.Location = new System.Drawing.Point(3, 3);
            this.btnAddKontakt.Name = "btnAddKontakt";
            this.btnAddKontakt.Size = new System.Drawing.Size(87, 23);
            this.btnAddKontakt.TabIndex = 4;
            this.btnAddKontakt.Text = "Dodaj Kontakt";
            this.btnAddKontakt.Click += new System.EventHandler(this.btnAddKontakt_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(217)))), ((int)(((byte)(247)))));
            this.tabControl1.CanReorderTabs = false;
            this.tabControl1.Controls.Add(this.tabControlPanel2);
            this.tabControl1.Controls.Add(this.tabControlPanel1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 211);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedTabFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.tabControl1.SelectedTabIndex = 1;
            this.tabControl1.Size = new System.Drawing.Size(905, 406);
            this.tabControl1.TabIndex = 5;
            this.tabControl1.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox;
            this.tabControl1.Tabs.Add(this.tabItem2);
            this.tabControl1.Tabs.Add(this.tabItem1);
            this.tabControl1.Text = "tabControl1";
            this.tabControl1.SelectedTabChanged += new DevComponents.DotNetBar.TabStrip.SelectedTabChangedEventHandler(this.tabControl1_SelectedTabChanged);
            this.tabControl1.Click += new System.EventHandler(this.tabControl1_Click);
            // 
            // tabControlPanel2
            // 
            this.tabControlPanel2.Controls.Add(this.dataGridView1);
            this.tabControlPanel2.Controls.Add(this.labelX2);
            this.tabControlPanel2.Controls.Add(this.dgPrzeglady);
            this.tabControlPanel2.Controls.Add(this.labelX1);
            this.tabControlPanel2.Controls.Add(this.panel3);
            this.tabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel2.Location = new System.Drawing.Point(0, 26);
            this.tabControlPanel2.Name = "tabControlPanel2";
            this.tabControlPanel2.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel2.Size = new System.Drawing.Size(905, 380);
            this.tabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(179)))), ((int)(((byte)(231)))));
            this.tabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.tabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.tabControlPanel2.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel2.Style.GradientAngle = 90;
            this.tabControlPanel2.TabIndex = 2;
            this.tabControlPanel2.TabItem = this.tabItem2;
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(217)))), ((int)(((byte)(247)))));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dataGridView1.Location = new System.Drawing.Point(1, 197);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(903, 182);
            this.dataGridView1.TabIndex = 12;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
            // 
            // labelX2
            // 
            this.labelX2.BackColor = System.Drawing.Color.Transparent;
            this.labelX2.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelX2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelX2.Location = new System.Drawing.Point(1, 174);
            this.labelX2.Name = "labelX2";
            this.labelX2.Size = new System.Drawing.Size(903, 23);
            this.labelX2.TabIndex = 11;
            this.labelX2.Text = "Przegl�dy Wykonane";
            // 
            // dgPrzeglady
            // 
            this.dgPrzeglady.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(194)))), ((int)(((byte)(217)))), ((int)(((byte)(247)))));
            this.dgPrzeglady.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPrzeglady.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgPrzeglady.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(215)))), ((int)(((byte)(229)))));
            this.dgPrzeglady.Location = new System.Drawing.Point(1, 52);
            this.dgPrzeglady.Name = "dgPrzeglady";
            this.dgPrzeglady.Size = new System.Drawing.Size(903, 122);
            this.dgPrzeglady.TabIndex = 0;
            this.dgPrzeglady.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPrzeglady_CellClick);
            this.dgPrzeglady.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPrzeglady_CellDoubleClick);
            // 
            // labelX1
            // 
            this.labelX1.BackColor = System.Drawing.Color.Transparent;
            this.labelX1.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelX1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelX1.Location = new System.Drawing.Point(1, 29);
            this.labelX1.Name = "labelX1";
            this.labelX1.Size = new System.Drawing.Size(903, 23);
            this.labelX1.TabIndex = 11;
            this.labelX1.Text = "Przegl�dy Do Wykonania";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.btnDodajPrzeglad);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(1, 1);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(903, 28);
            this.panel3.TabIndex = 10;
            // 
            // btnDodajPrzeglad
            // 
            this.btnDodajPrzeglad.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnDodajPrzeglad.Location = new System.Drawing.Point(3, 3);
            this.btnDodajPrzeglad.Name = "btnDodajPrzeglad";
            this.btnDodajPrzeglad.Size = new System.Drawing.Size(103, 23);
            this.btnDodajPrzeglad.TabIndex = 9;
            this.btnDodajPrzeglad.Text = "Dodaj Przegl�d";
            this.btnDodajPrzeglad.Click += new System.EventHandler(this.btnDodajPrzeglad_Click);
            // 
            // tabItem2
            // 
            this.tabItem2.AttachedControl = this.tabControlPanel2;
            this.tabItem2.Name = "tabItem2";
            this.tabItem2.Text = "Przegl�dy";
            this.tabItem2.Click += new System.EventHandler(this.tabItem2_Click);
            // 
            // tabControlPanel1
            // 
            this.tabControlPanel1.Controls.Add(this.dgKontakty);
            this.tabControlPanel1.Controls.Add(this.panel2);
            this.tabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlPanel1.Location = new System.Drawing.Point(0, 26);
            this.tabControlPanel1.Name = "tabControlPanel1";
            this.tabControlPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tabControlPanel1.Size = new System.Drawing.Size(905, 380);
            this.tabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(142)))), ((int)(((byte)(179)))), ((int)(((byte)(231)))));
            this.tabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.tabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.tabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(97)))), ((int)(((byte)(156)))));
            this.tabControlPanel1.Style.BorderSide = ((DevComponents.DotNetBar.eBorderSide)(((DevComponents.DotNetBar.eBorderSide.Left | DevComponents.DotNetBar.eBorderSide.Right)
                        | DevComponents.DotNetBar.eBorderSide.Bottom)));
            this.tabControlPanel1.Style.GradientAngle = 90;
            this.tabControlPanel1.TabIndex = 1;
            this.tabControlPanel1.TabItem = this.tabItem1;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.btnAddKontakt);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(1, 1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(903, 28);
            this.panel2.TabIndex = 5;
            // 
            // tabItem1
            // 
            this.tabItem1.AttachedControl = this.tabControlPanel1;
            this.tabItem1.Name = "tabItem1";
            this.tabItem1.Text = "Kontakty";
            this.tabItem1.Click += new System.EventHandler(this.tabItem1_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupPanel4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(905, 211);
            this.panel1.TabIndex = 6;
            // 
            // groupPanel4
            // 
            this.groupPanel4.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel4.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel4.Controls.Add(this.groupPanel3);
            this.groupPanel4.Controls.Add(this.groupPanel2);
            this.groupPanel4.Controls.Add(this.tboxFakturaNrMieszkania);
            this.groupPanel4.Controls.Add(this.button6);
            this.groupPanel4.Controls.Add(this.tboxFakturaNrDomu);
            this.groupPanel4.Controls.Add(this.label19);
            this.groupPanel4.Controls.Add(this.label11);
            this.groupPanel4.Controls.Add(this.tboxnrMieszkania);
            this.groupPanel4.Controls.Add(this.label18);
            this.groupPanel4.Controls.Add(this.label10);
            this.groupPanel4.Controls.Add(this.tboxNrDomu);
            this.groupPanel4.Location = new System.Drawing.Point(3, 3);
            this.groupPanel4.Name = "groupPanel4";
            this.groupPanel4.Size = new System.Drawing.Size(901, 200);
            // 
            // 
            // 
            this.groupPanel4.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.groupPanel4.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderBottomWidth = 1;
            this.groupPanel4.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel4.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderLeftWidth = 1;
            this.groupPanel4.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderRightWidth = 1;
            this.groupPanel4.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel4.Style.BorderTopWidth = 1;
            this.groupPanel4.Style.CornerDiameter = 4;
            this.groupPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel4.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel4.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel4.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            this.groupPanel4.TabIndex = 17;
            // 
            // groupPanel3
            // 
            this.groupPanel3.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel3.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel3.Controls.Add(this.label4);
            this.groupPanel3.Controls.Add(this.label3);
            this.groupPanel3.Controls.Add(this.panel4);
            this.groupPanel3.Controls.Add(this.label5);
            this.groupPanel3.Controls.Add(this.label1);
            this.groupPanel3.Controls.Add(this.label2);
            this.groupPanel3.Location = new System.Drawing.Point(458, 3);
            this.groupPanel3.Name = "groupPanel3";
            this.groupPanel3.Size = new System.Drawing.Size(442, 157);
            // 
            // 
            // 
            this.groupPanel3.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.groupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderBottomWidth = 1;
            this.groupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderLeftWidth = 1;
            this.groupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderRightWidth = 1;
            this.groupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel3.Style.BorderTopWidth = 1;
            this.groupPanel3.Style.CornerDiameter = 4;
            this.groupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            this.groupPanel3.TabIndex = 16;
            this.groupPanel3.Text = "Faktura";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(3, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Nazwa:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(5, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Miasto:";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.Controls.Add(this.tboxFakturaNazwa);
            this.panel4.Controls.Add(this.tboxFakturaUlica);
            this.panel4.Controls.Add(this.tboxFakturaMiasto);
            this.panel4.Controls.Add(this.tboxFakturaKod);
            this.panel4.Controls.Add(this.tboxNIP);
            this.panel4.Location = new System.Drawing.Point(136, 2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(298, 191);
            this.panel4.TabIndex = 15;
            // 
            // tboxFakturaNazwa
            // 
            this.tboxFakturaNazwa.Location = new System.Drawing.Point(3, 1);
            this.tboxFakturaNazwa.MaxLength = 200;
            this.tboxFakturaNazwa.Name = "tboxFakturaNazwa";
            this.tboxFakturaNazwa.Size = new System.Drawing.Size(290, 20);
            this.tboxFakturaNazwa.TabIndex = 0;
            this.tboxFakturaNazwa.Tag = "Wpisz Nazw�do faktury\n";
            // 
            // tboxFakturaUlica
            // 
            this.tboxFakturaUlica.Location = new System.Drawing.Point(3, 27);
            this.tboxFakturaUlica.MaxLength = 200;
            this.tboxFakturaUlica.Name = "tboxFakturaUlica";
            this.tboxFakturaUlica.Size = new System.Drawing.Size(290, 20);
            this.tboxFakturaUlica.TabIndex = 1;
            this.tboxFakturaUlica.Tag = "Wpisz Ulic�\n";
            // 
            // tboxFakturaMiasto
            // 
            this.tboxFakturaMiasto.Location = new System.Drawing.Point(3, 53);
            this.tboxFakturaMiasto.MaxLength = 200;
            this.tboxFakturaMiasto.Name = "tboxFakturaMiasto";
            this.tboxFakturaMiasto.Size = new System.Drawing.Size(290, 20);
            this.tboxFakturaMiasto.TabIndex = 4;
            this.tboxFakturaMiasto.Tag = "Wpisz Miasto\n";
            // 
            // tboxFakturaKod
            // 
            this.tboxFakturaKod.Location = new System.Drawing.Point(3, 79);
            this.tboxFakturaKod.MaxLength = 20;
            this.tboxFakturaKod.Name = "tboxFakturaKod";
            this.tboxFakturaKod.Size = new System.Drawing.Size(290, 20);
            this.tboxFakturaKod.TabIndex = 5;
            this.tboxFakturaKod.Tag = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(3, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Adres:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(5, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Kod Pocztowy:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(5, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "NIP:";
            // 
            // groupPanel2
            // 
            this.groupPanel2.BackColor = System.Drawing.Color.Transparent;
            this.groupPanel2.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.label6);
            this.groupPanel2.Controls.Add(this.label7);
            this.groupPanel2.Controls.Add(this.label8);
            this.groupPanel2.Controls.Add(this.panelNewFirm);
            this.groupPanel2.Controls.Add(this.label9);
            this.groupPanel2.Controls.Add(this.label20);
            this.groupPanel2.Location = new System.Drawing.Point(3, 3);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(449, 157);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(237)))), ((int)(((byte)(254)))));
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            this.groupPanel2.TabIndex = 16;
            this.groupPanel2.Text = "Protok�";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(3, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Nazwa:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(3, 58);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Miasto:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(3, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Adres:";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // panelNewFirm
            // 
            this.panelNewFirm.BackColor = System.Drawing.Color.Transparent;
            this.panelNewFirm.Controls.Add(this.combTyp);
            this.panelNewFirm.Controls.Add(this.tboxMiasto);
            this.panelNewFirm.Controls.Add(this.tboxKodPocztowy);
            this.panelNewFirm.Controls.Add(this.tboxNazwa);
            this.panelNewFirm.Controls.Add(this.tboxUlica);
            this.panelNewFirm.Location = new System.Drawing.Point(142, 2);
            this.panelNewFirm.Name = "panelNewFirm";
            this.panelNewFirm.Size = new System.Drawing.Size(296, 191);
            this.panelNewFirm.TabIndex = 14;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(3, 84);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Kod Pocztowy:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Location = new System.Drawing.Point(3, 110);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(78, 13);
            this.label20.TabIndex = 6;
            this.label20.Text = "Typ Przegl�du:";
            // 
            // tboxFakturaNrMieszkania
            // 
            this.tboxFakturaNrMieszkania.Location = new System.Drawing.Point(512, 194);
            this.tboxFakturaNrMieszkania.Name = "tboxFakturaNrMieszkania";
            this.tboxFakturaNrMieszkania.Size = new System.Drawing.Size(167, 20);
            this.tboxFakturaNrMieszkania.TabIndex = 3;
            this.tboxFakturaNrMieszkania.Tag = "Wpisz Numer domu\n";
            this.tboxFakturaNrMieszkania.Visible = false;
            // 
            // tboxFakturaNrDomu
            // 
            this.tboxFakturaNrDomu.Location = new System.Drawing.Point(512, 169);
            this.tboxFakturaNrDomu.Name = "tboxFakturaNrDomu";
            this.tboxFakturaNrDomu.Size = new System.Drawing.Size(167, 20);
            this.tboxFakturaNrDomu.TabIndex = 2;
            this.tboxFakturaNrDomu.Tag = "Wpisz Numer domu\n";
            this.tboxFakturaNrDomu.Visible = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Location = new System.Drawing.Point(20, 169);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(72, 13);
            this.label19.TabIndex = 6;
            this.label19.Text = "Numer Domu:";
            this.label19.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(436, 198);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(97, 13);
            this.label11.TabIndex = 6;
            this.label11.Text = "Numer Mieszkania:";
            this.label11.Visible = false;
            // 
            // tboxnrMieszkania
            // 
            this.tboxnrMieszkania.Location = new System.Drawing.Point(130, 189);
            this.tboxnrMieszkania.Name = "tboxnrMieszkania";
            this.tboxnrMieszkania.Size = new System.Drawing.Size(290, 20);
            this.tboxnrMieszkania.TabIndex = 3;
            this.tboxnrMieszkania.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Location = new System.Drawing.Point(436, 173);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(72, 13);
            this.label18.TabIndex = 6;
            this.label18.Text = "Numer Domu:";
            this.label18.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(20, 192);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(97, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Numer Mieszkania:";
            this.label10.Visible = false;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(905, 617);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.MinimizeBox = false;
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Zmiana Danych";
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgKontakty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabControl1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabControlPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgPrzeglady)).EndInit();
            this.panel3.ResumeLayout(false);
            this.tabControlPanel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupPanel4.ResumeLayout(false);
            this.groupPanel4.PerformLayout();
            this.groupPanel3.ResumeLayout(false);
            this.groupPanel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.groupPanel2.ResumeLayout(false);
            this.groupPanel2.PerformLayout();
            this.panelNewFirm.ResumeLayout(false);
            this.panelNewFirm.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX button6;
        private System.Windows.Forms.ComboBox combTyp;
        private System.Windows.Forms.TextBox tboxNIP;
        private System.Windows.Forms.TextBox tboxKodPocztowy;
        private System.Windows.Forms.TextBox tboxNrDomu;
        private System.Windows.Forms.TextBox tboxMiasto;
        private System.Windows.Forms.TextBox tboxUlica;
        private System.Windows.Forms.TextBox tboxNazwa;
        private System.Windows.Forms.DataGridView dgKontakty;
        private DevComponents.DotNetBar.ButtonX btnAddKontakt;
        private DevComponents.DotNetBar.TabControl tabControl1;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel1;
        private DevComponents.DotNetBar.TabItem tabItem1;
        private DevComponents.DotNetBar.TabControlPanel tabControlPanel2;
        private DevComponents.DotNetBar.TabItem tabItem2;
        private DevComponents.DotNetBar.ButtonX btnDodajPrzeglad;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private System.Windows.Forms.DataGridView dgPrzeglady;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel4;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox tboxFakturaNazwa;
        private System.Windows.Forms.TextBox tboxFakturaUlica;
        private System.Windows.Forms.TextBox tboxFakturaNrDomu;
        private System.Windows.Forms.TextBox tboxFakturaMiasto;
        private System.Windows.Forms.TextBox tboxFakturaKod;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label18;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panelNewFirm;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tboxFakturaNrMieszkania;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tboxnrMieszkania;
        private System.Windows.Forms.Label label10;

    }
}