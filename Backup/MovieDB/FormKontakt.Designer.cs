namespace AppSpace
{
    partial class FormKontakt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.kontakt1 = new AppSpace.Kontakt();
            this.SuspendLayout();
            // 
            // kontakt1
            // 
            this.kontakt1.IDFirma = 0;
            this.kontakt1.IDKONTAKT = 0;
            this.kontakt1.Location = new System.Drawing.Point(3, 4);
            this.kontakt1.Name = "kontakt1";
            this.kontakt1.Size = new System.Drawing.Size(454, 237);
            this.kontakt1.TabIndex = 0;
            this.kontakt1.TRYB = AppSpace.Kontakt.TrybEnum.Nowy;
            // 
            // FormKontakt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(466, 238);
            this.Controls.Add(this.kontakt1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(474, 272);
            this.MinimumSize = new System.Drawing.Size(474, 272);
            this.Name = "FormKontakt";
            this.Text = "Kontakt - Dodawanie / Edycja";
            this.ResumeLayout(false);

        }

        #endregion

        public Kontakt kontakt1;

    }
}