namespace AppSpace
{
    partial class NowyPrzeglad
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSave = new DevComponents.DotNetBar.ButtonX();
            this.dtpDataPrzegladu = new System.Windows.Forms.DateTimePicker();
            this.lblNazwaFirmy = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tboxUwagi = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.btnSave.Location = new System.Drawing.Point(317, 169);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(73, 23);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Zapisz";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dtpDataPrzegladu
            // 
            this.dtpDataPrzegladu.CustomFormat = "yyyyMMMMMMMMMMMMMMM";
            this.dtpDataPrzegladu.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDataPrzegladu.Location = new System.Drawing.Point(106, 30);
            this.dtpDataPrzegladu.Name = "dtpDataPrzegladu";
            this.dtpDataPrzegladu.Size = new System.Drawing.Size(284, 20);
            this.dtpDataPrzegladu.TabIndex = 7;
            // 
            // lblNazwaFirmy
            // 
            this.lblNazwaFirmy.AutoSize = true;
            this.lblNazwaFirmy.Location = new System.Drawing.Point(103, 10);
            this.lblNazwaFirmy.Name = "lblNazwaFirmy";
            this.lblNazwaFirmy.Size = new System.Drawing.Size(64, 13);
            this.lblNazwaFirmy.TabIndex = 6;
            this.lblNazwaFirmy.Text = "Nazwa firmy";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Nazwa firmy";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Uwagi";
            // 
            // tboxUwagi
            // 
            this.tboxUwagi.Location = new System.Drawing.Point(106, 57);
            this.tboxUwagi.Multiline = true;
            this.tboxUwagi.Name = "tboxUwagi";
            this.tboxUwagi.Size = new System.Drawing.Size(284, 106);
            this.tboxUwagi.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Data Przegl�du";
            // 
            // NowyPrzeglad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tboxUwagi);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.dtpDataPrzegladu);
            this.Controls.Add(this.lblNazwaFirmy);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "NowyPrzeglad";
            this.Size = new System.Drawing.Size(408, 195);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevComponents.DotNetBar.ButtonX btnSave;
        private System.Windows.Forms.DateTimePicker dtpDataPrzegladu;
        private System.Windows.Forms.Label lblNazwaFirmy;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tboxUwagi;
        private System.Windows.Forms.Label label2;
    }
}
