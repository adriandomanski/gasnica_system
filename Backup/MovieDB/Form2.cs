using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace AppSpace
{
    public partial class Form2 : DevComponents.DotNetBar.Office2007Form
    {
        public string NAZWA, ULICA, NR_DOMU, NR_MIESZKANIA, MIASTO, KOD_POCZTOWY, NIP, ID_TYP_PRZEDLADU, FAKTURA_NAZWA,
                        FAKTURA_ULICA,
                            FAKTURA_MIASTO,
                                FAKTURA_NR_DOMU,
                                    FAKTURA_KOD_POCZTOWY,
            FAKTURA_NR_MIESZKANIA;

        DataGridViewButtonColumn editButton;
        DataGridViewButtonColumn deleteButton;

        DataGridViewButtonColumn editButton1;
        DataGridViewButtonColumn deleteButton1;


        DataGridViewButtonColumn editButton2;
        DataGridViewButtonColumn deleteButton2;


        public string year, publisher, title, previewed, type;
        public int movieID, ID_FIRM;
        public Form2()
        {
            InitializeComponent();
            TYPPrzegladu tp;
            tp = new TYPPrzegladu();
            tp.ID = "1";
            tp.NAME = "Roczny";

            combTyp.Items.Add(tp);
            tp = new TYPPrzegladu();
            tp.ID = "2";
            tp.NAME = "P�roczny";

            combTyp.Items.Add(tp);
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            tboxKodPocztowy.Text = KOD_POCZTOWY;
            tboxMiasto.Text = MIASTO;
            tboxNazwa.Text = NAZWA;
            tboxNIP.Text = NIP;
            tboxNrDomu.Text = NR_DOMU;
            tboxUlica.Text = ULICA;
            tboxFakturaNazwa.Text = FAKTURA_NAZWA;
            tboxFakturaUlica.Text = FAKTURA_ULICA;
            tboxFakturaMiasto.Text = FAKTURA_MIASTO;
            tboxFakturaNrDomu.Text = FAKTURA_NR_DOMU;
            tboxFakturaKod.Text = FAKTURA_KOD_POCZTOWY;
            tboxnrMieszkania.Text = NR_MIESZKANIA;
            tboxFakturaNrMieszkania.Text = FAKTURA_NR_MIESZKANIA;

            foreach (TYPPrzegladu var in combTyp.Items)
            {
                if (var.ID == ID_TYP_PRZEDLADU)
                {
                    combTyp.SelectedItem = var;
                    break;
                }
            }
            tabControl1.SelectedTab = tabItem2;
            SetDatakontakty();
            tabItem2_Click(null, null);
        }

        #region Update
        private void button6_Click(object sender, EventArgs e)
        {
            //sql query 
            Form1 f1 = new Form1();
            string typeString;


            string SQLUpdateString;
            SQLUpdateString = "UPDATE FIRMA SET NAZWA ='" + tboxNazwa.Text.Replace("'", "''") +
                "', ULICA='" + tboxUlica.Text.Replace("'", "''") +
              //  "', NR_DOMU='" + tboxNrDomu.Text.Replace("'", "''") +
                "', MIASTO='" + tboxMiasto.Text.Replace("'", "''") +
                "', KOD_POCZTOWY='" + tboxKodPocztowy.Text.Replace("'", "''") +
                "', NIP='" + tboxNIP.Text.Replace("'", "''") +
              //   "', NR_MIESZKANIA='" + tboxnrMieszkania.Text.Replace("'", "''") +

                "', FAKTURA_NAZWA='" + tboxFakturaNazwa.Text.Replace("'", "''") +
                "', FAKTURA_ULICA='" + tboxFakturaUlica.Text.Replace("'", "''") +
                "', FAKTURA_MIASTO='" + tboxFakturaMiasto.Text.Replace("'", "''") +
              //  "', FAKTURA_NR_DOMU='" + tboxFakturaNrDomu.Text.Replace("'", "''") +
                "', FAKTURA_KOD_POCZTOWY='" + tboxFakturaKod.Text.Replace("'", "''") +
              //   "', FAKTURA_NR_MIESZKANIA='" + tboxFakturaNrMieszkania.Text.Replace("'", "''") +

                "', ID_TYPU_PRZEGLADU=" + ((TYPPrzegladu)combTyp.SelectedItem).ID +
                " WHERE ID=" + ID_FIRM + ";";

            OleDbCommand SQLCommand = new OleDbCommand();
            SQLCommand.CommandText = SQLUpdateString;
            SQLCommand.Connection = f1.database;
            int response = SQLCommand.ExecuteNonQuery();
            MessageBox.Show("Zmiany Zapisane!", "INFO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Saved(this, new EventArgs());
        }

        #endregion

        private void button6_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button6_Click(null, null);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
        public delegate void SavedHandler(object obj, EventArgs eventArgs);
        /// <summary>
        /// zdarzenie po zapisaniu danych
        /// </summary>
        public event SavedHandler Saved;

        protected void OnSaved(object obj, EventArgs eventArgs)
        {
            if (Saved != null)
            {
                Saved(this, eventArgs);
            }
        }

        private void SetDatakontakty()
        {
            dgKontakty.DataSource = null;
            dgKontakty.Refresh();
            //
            //string queryString = "SELECT movieID, Title, Publisher, Previewed, MovieYear, Type FROM movie,movieType WHERE movietype.typeID = movie.typeID";
            string queryString = "SELECT PRZEDSTAWICIEL.ID, PRZEDSTAWICIEL.IMIE, PRZEDSTAWICIEL.NAZWISKO, PRZEDSTAWICIEL.TELEFON_1, PRZEDSTAWICIEL.TELEFON_2, PRZEDSTAWICIEL.TELEFON_3 as stanowisko_funkcja FROM PRZEDSTAWICIEL WHERE PRZEDSTAWICIEL.ID_FIRMA=" + ID_FIRM + ";";


            Form1 f1 = new Form1();

            OleDbCommand SQLQuery = new OleDbCommand();
            DataTable data = null;
            dgKontakty.DataSource = null;
            SQLQuery.Connection = null;
            OleDbDataAdapter dataAdapter = null;
            dgKontakty.Columns.Clear(); // <-- clear columns

            //---------------------------------
            SQLQuery.CommandText = queryString;
            SQLQuery.Connection = f1.database;
            data = new DataTable();
            dataAdapter = new OleDbDataAdapter(SQLQuery);
            dataAdapter.Fill(data);
            dgKontakty.DataSource = data;
            dgKontakty.AllowUserToAddRows = false; // remove the null line
            dgKontakty.ReadOnly = true;
            dgKontakty.Columns[0].Visible = false;
            //editButton = new DataGridViewButtonColumn();
            //editButton.HeaderText = "Edytuj / Podgl�d";
            //editButton.Text = "Edytuj / Podgl�d";
            //editButton.UseColumnTextForButtonValue = true;
            //editButton.Width = 100;
            //dgKontakty.Columns.Add(editButton);
            // insert delete button to datagridview
            deleteButton = new DataGridViewButtonColumn();
            deleteButton.HeaderText = "Usu�";
            deleteButton.Text = "Usu�";
            deleteButton.UseColumnTextForButtonValue = true;
            deleteButton.Width = 70;
            dgKontakty.Columns.Add(deleteButton);
            dgKontakty.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

        }
        FormKontakt fk;
        private void btnAddKontakt_Click(object sender, EventArgs e)
        {
            fk = new FormKontakt();
            fk.kontakt1.IDFirma = ID_FIRM;
            fk.Disposed += new EventHandler(fk_Disposed);
            fk.ShowDialog();
        }

        void fk_Disposed(object sender, EventArgs e)
        {
            SetDatakontakty();
        }

        private void dgKontakty_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {



            Form1 f1 = new Form1();
            int IDKONTAKTINT = 0;
            //  string queryString = "SELECT movieID, Title, Publisher, Previewed, MovieYear, Type FROM movie,movieType WHERE movietype.typeID = movie.typeID";
            // string queryString = "SELECT T.ID, T.NAZWA, T.ULICA, T.NR_DOMU, T.NR_MIESZKANIA, T.MIASTO,T.KOD_POCZTOWY,T.NIP, TP.NAZWA, TP.ID FROM TYP_PRZEGLADU TP ,FIRMA T WHERE TP.ID = T.ID_TYPU_PRZEGLADU";
            //string queryString = "SELECT PRZEDSTAWICIEL.ID, PRZEDSTAWICIEL.IMIE, PRZEDSTAWICIEL.NAZWISKO, PRZEDSTAWICIEL.TELEFON_1, PRZEDSTAWICIEL.TELEFON_2, PRZEDSTAWICIEL.TELEFON_3 FROM PRZEDSTAWICIEL WHERE PRZEDSTAWICIEL.ID_FIRMA=" + ID_FIRM + ";";

            int currentRow = int.Parse(e.RowIndex.ToString());
            try
            {
                string IDKONTAKTstr = dgKontakty[0, currentRow].Value.ToString();
                IDKONTAKTINT = int.Parse(IDKONTAKTstr);
            }
            catch (Exception ex) { }
            // edit button
            //if (dgKontakty.Columns[e.ColumnIndex] == editButton && currentRow >= 0)
            //{
            //    fk = new FormKontakt();
            //    fk.kontakt1.TRYB = Kontakt.TrybEnum.Edycja;
            //    fk.kontakt1.IDFirma = ID_FIRM;
            //    fk.kontakt1.SetCOntrolByStrings(
            //        dgKontakty.SelectedRows[0].Cells[1].Value.ToString(),
            //        dgKontakty.SelectedRows[0].Cells[2].Value.ToString(),
            //        dgKontakty.SelectedRows[0].Cells[3].Value.ToString(),
            //        dgKontakty.SelectedRows[0].Cells[4].Value.ToString(),
            //        dgKontakty.SelectedRows[0].Cells[5].Value.ToString(),
            //        Convert.ToInt32(dgKontakty.SelectedRows[0].Cells[0].Value.ToString()));

            //    fk.Disposed += new EventHandler(fk_Disposed);
            //    fk.ShowDialog();

            //}
            // delete button
            //else
            if (dgKontakty.Columns[e.ColumnIndex] == deleteButton && currentRow >= 0 && IDKONTAKTINT != 0)
            {
                if (DialogResult.Yes == DevComponents.DotNetBar.MessageBoxEx.Show("Czy napewno chcesz usun��?", "Pytanie", MessageBoxButtons.YesNo))
                {
                    dgKontakty.Visible = false;
                    // dgKontakty.DataSource = null;
                    // delete sql query
                    dgKontakty.Rows.Remove(dgKontakty.CurrentRow);
                    string queryDeleteString = "DELETE FROM PRZEDSTAWICIEL where ID = " + IDKONTAKTINT + "";
                    OleDbCommand sqlDelete = new OleDbCommand();
                    sqlDelete.CommandText = queryDeleteString;
                    sqlDelete.Connection = f1.database;
                    int rr = sqlDelete.ExecuteNonQuery();
                    if (rr == 1)
                    {
                        dgKontakty_CellContentClick(sender, e);
                    }
                    dgKontakty.DataSource = null;
                    //dgKontakty.Rows.Clear();
                    f1.database.ResetState();
                    SetDatakontakty();
                    dgKontakty.Refresh();
                    //fk = new FormKontakt();
                    //fk.Disposed+=new EventHandler(fk_Disposed);
                    //fk.Dispose();
                    dgKontakty.Visible = true;
                }

            }
        }

        private void buttonX1_Click(object sender, EventArgs e)
        {
            SetDatakontakty();
        }

        private void tabItem2_Click(object sender, EventArgs e)
        {
            string queryString0 = "SELECT pg.id, pg.data_przegladu, pg.nr_protokolu, pg.nr_faktury, pg.data_faktury,Month ( pg.Miesiac_przegladu )as miesiac, Year (  pg.Miesiac_przegladu )as rok , pg.uwagi FROM PRZEGLAD AS pg WHERE pg.ID_FIRMA=" + ID_FIRM + " and pg.zrobiony = " + false.ToString() + ";";
            SetPrzeglady(false, dgPrzeglady, queryString0);
            dgPrzeglady.Visible = true;
            for (int i = 0; i < 5; i++)
            {
                dgPrzeglady.Columns[i].Visible = false;
            }
            dgPrzeglady.Columns[5].Width = 50;
            dgPrzeglady.Columns[6].Width = 110;
            dgPrzeglady.Columns[7].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            string queryString2 = "SELECT pg.id, pg.data_przegladu, pg.nr_protokolu, pg.nr_faktury, pg.data_faktury, pg.uwagi FROM PRZEGLAD AS pg WHERE pg.ID_FIRMA=" + ID_FIRM + " and pg.zrobiony = " + true.ToString() + ";";
            SetPrzeglady(true, dataGridView1, queryString2);
            dataGridView1.Columns[5].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            return;
            //string queryString = "SELECT pg.id, pg.data_przegladu, pg.zrobiony, pg.nr_protokolu, pg.nr_faktury, pg.data_faktury, pg.uwagi FROM PRZEGLAD AS pg WHERE pg.ID_FIRMA=" + ID_FIRM + ";";


            //Form1 f1 = new Form1();

            //OleDbCommand SQLQuery = new OleDbCommand();
            //DataTable data = null;
            //dgPrzeglady.DataSource = null;
            //SQLQuery.Connection = null;
            //OleDbDataAdapter dataAdapter = null;
            //dgPrzeglady.Columns.Clear(); // <-- clear columns

            ////---------------------------------
            //SQLQuery.CommandText = queryString;
            //SQLQuery.Connection = f1.database;
            //data = new DataTable();
            //dataAdapter = new OleDbDataAdapter(SQLQuery);
            //dataAdapter.Fill(data);
            //dgPrzeglady.DataSource = data;
            //dgPrzeglady.AllowUserToAddRows = false; // remove the null line
            //dgPrzeglady.ReadOnly = true;
            //dgPrzeglady.Columns[0].Visible = false;
            //editButton = new DataGridViewButtonColumn();
            //editButton.HeaderText = "Edytuj / Podgl�d";
            //editButton.Text = "Edytuj / Podgl�d";
            //editButton.UseColumnTextForButtonValue = true;
            //editButton.Width = 100;
            //dgPrzeglady.Columns.Add(editButton);
            //// insert delete button to datagridview
            //deleteButton = new DataGridViewButtonColumn();
            //deleteButton.HeaderText = "Usu�";
            //deleteButton.Text = "Usu�";
            //deleteButton.UseColumnTextForButtonValue = true;
            //deleteButton.Width = 70;
            //dgPrzeglady.Columns.Add(deleteButton);


            //     loadDataGrid(queryString, dgPrzeglady);
        }


        private void SetPrzeglady(bool wykonane, DataGridView datagrid, string queryString)
        {
            //string queryString = "SELECT pg.id, pg.data_przegladu, pg.nr_protokolu, pg.nr_faktury, pg.data_faktury, pg.uwagi FROM PRZEGLAD AS pg WHERE pg.ID_FIRMA=" + ID_FIRM+" and pg.zrobiony = "+ wykonane+ ";";


            Form1 f1 = new Form1();

            OleDbCommand SQLQuery = new OleDbCommand();
            DataTable data = null;
            //datagrid.Rows.Clear();
            datagrid.DataSource = null;
            SQLQuery.Connection = null;
            OleDbDataAdapter dataAdapter = null;
            datagrid.Columns.Clear(); // <-- clear columns

            //---------------------------------
            SQLQuery.CommandText = queryString;
            SQLQuery.Connection = f1.database;
            data = new DataTable();
            dataAdapter = new OleDbDataAdapter(SQLQuery);
            dataAdapter.Fill(data);
            datagrid.DataSource = data;
            datagrid.AllowUserToAddRows = false; // remove the null line
            datagrid.ReadOnly = true;
            datagrid.Columns[0].Visible = false;
            if (wykonane)
            {
                //editButton = new DataGridViewButtonColumn();
                //editButton.HeaderText = "Edytuj / Podgl�d";
                //editButton.Text = "Edytuj / Podgl�d";
                //editButton.UseColumnTextForButtonValue = true;
                //editButton.Width = 100;
                //datagrid.Columns.Add(editButton);
                 //insert delete button to datagridview
                deleteButton = new DataGridViewButtonColumn();
                deleteButton.HeaderText = "Usu�";
                deleteButton.Text = "Usu�";
                deleteButton.UseColumnTextForButtonValue = true;
                deleteButton.Width = 70;
                datagrid.Columns.Add(deleteButton);
            }
            else
            {
                //editButton1 = new DataGridViewButtonColumn();
                //editButton1.HeaderText = "Edytuj / Podgl�d";
                //editButton1.Text = "Edytuj / Podgl�d";
                //editButton1.UseColumnTextForButtonValue = true;
                //editButton1.Width = 100;
                //datagrid.Columns.Add(editButton1);
                // insert delete button to datagridview
                deleteButton1 = new DataGridViewButtonColumn();
                deleteButton1.HeaderText = "Usu�";
                deleteButton1.Text = "Usu�";
                deleteButton1.UseColumnTextForButtonValue = true;
                deleteButton1.Width = 70;
                datagrid.Columns.Add(deleteButton1);
            }


            //     loadDataGrid(queryString, dgPrzeglady);
        }
        private void tabItem1_Click(object sender, EventArgs e)
        {
            SetDatakontakty();
        }

        private void tabControl1_Click(object sender, EventArgs e)
        {

        }

        private void btnDodajPrzeglad_Click(object sender, EventArgs e)
        {
            PrzegladFOrm frm = new PrzegladFOrm();
            frm.nowyPrzeglad1.DataPrzegladu = DateTime.Now;
            frm.nowyPrzeglad1.IDTypPrzegladu = Convert.ToInt32(ID_TYP_PRZEDLADU);
            frm.Disposed += new EventHandler(frm_Disposed);
            frm.TRYB = PrzegladFOrm.TrybENum.Dodawanie;
            frm.nowyPrzeglad1.IDFIRMY = ID_FIRM;
            frm.nowyPrzeglad1.NazwaFirmy = NAZWA;
            frm.ShowDialog();
        }

        void frm_Disposed(object sender, EventArgs e)
        {
            tabItem2_Click(null, null);// throw new Exception("The method or operation is not implemented.");
        }

        private void dgPrzeglady_CellClick(object sender, DataGridViewCellEventArgs e)
        {


            if (e.RowIndex > -1 && e.ColumnIndex == deleteButton1.Index)
            {
                if (DialogResult.Yes == DevComponents.DotNetBar.MessageBoxEx.Show("czy napewno chcesz usun�� przegl�d?", "Pytanie", MessageBoxButtons.YesNo))
                {
                    Form1 f1 = new Form1();
                    int idPrzeglad = Convert.ToInt32(dgPrzeglady[0, e.RowIndex].Value.ToString());
                    dgPrzeglady.Visible = false;
                    // dgKontakty.DataSource = null;
                    // delete sql query
                    dgPrzeglady.Rows.Remove(dgPrzeglady.CurrentRow);
                    string queryDeleteString = "DELETE FROM PRZEGLAD where ID = " + idPrzeglad + "";
                    OleDbCommand sqlDelete = new OleDbCommand();
                    sqlDelete.CommandText = queryDeleteString;
                    sqlDelete.Connection = f1.database;
                    int rr = sqlDelete.ExecuteNonQuery();
                    if (rr != 1)
                    {
                        dgPrzeglady_CellClick(sender, e);
                    }
                    else
                    {
                        DevComponents.DotNetBar.MessageBoxEx.Show("Przegl�d zosta� usuni�ty");
                    }
                    dgPrzeglady.DataSource = null;
                    //dgKontakty.Rows.Clear();
                    f1.database.ResetState();
                    tabItem2_Click(null, null);
                    dgPrzeglady.Refresh();
                    dgPrzeglady.Visible = false;
                    //fk = new FormKontakt();
                    //fk.Disposed+=new EventHandler(fk_Disposed);
                    //fk.Dispose();
                    dgPrzeglady.Visible = true;
                    tabItem2_Click(null, null);
                }
            }
        }

        void fro_Disposed(object sender, EventArgs e)
        {
            tabItem2_Click(null, null);
        }

        private void tabControl1_SelectedTabChanged(object sender, DevComponents.DotNetBar.TabStripTabChangedEventArgs e)
        {
            
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.RowIndex > -1 && e.ColumnIndex == deleteButton.Index)
            {
                if (DialogResult.Yes == DevComponents.DotNetBar.MessageBoxEx.Show("czy napewno chcesz usun�� przegl�d?", "Pytanie", MessageBoxButtons.YesNo))
                {
                    Form1 f1 = new Form1();
                    int idPrzeglad = Convert.ToInt32(dataGridView1[0, e.RowIndex].Value.ToString());
                    dataGridView1.Visible = false;
                    // dgKontakty.DataSource = null;
                    // delete sql query
                    dataGridView1.Rows.Remove(dataGridView1.CurrentRow);
                    string queryDeleteString = "DELETE FROM PRZEGLAD where ID = " + idPrzeglad + "";
                    OleDbCommand sqlDelete = new OleDbCommand();
                    sqlDelete.CommandText = queryDeleteString;
                    sqlDelete.Connection = f1.database;
                    int rr = sqlDelete.ExecuteNonQuery();
                    if (rr != 1)
                    {
                        dgPrzeglady_CellClick(sender, e);
                    }
                    else
                    {
                        DevComponents.DotNetBar.MessageBoxEx.Show("Przegl�d zosta� usuni�ty");
                    }
                    dataGridView1.DataSource = null;
                    //dgKontakty.Rows.Clear();
                    f1.database.ResetState();
                    tabItem2_Click(null, null);
                    dataGridView1.Refresh();
                    //fk = new FormKontakt();
                    //fk.Disposed+=new EventHandler(fk_Disposed);
                    //fk.Dispose();
                    dataGridView1.Visible = true;

                    tabItem2_Click(null, null);
                }
            }
        }

        private void dgPrzeglady_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1 && e.ColumnIndex != deleteButton1.Index)
            {
                PrzegladFOrm fro = new PrzegladFOrm();
                fro.TRYB = PrzegladFOrm.TrybENum.Edycja;
                fro.przeglad1.idPrzeglad = Convert.ToInt32(dgPrzeglady[0, e.RowIndex].Value.ToString());
                fro.przeglad1.dataPrzegladu = dgPrzeglady[1, e.RowIndex].Value.ToString();
                //   fro.przeglad1.zrobiony = Convert.ToBoolean(dgPrzeglady[2, e.RowIndex].Value.ToString());
                fro.przeglad1.nrProtokolu = dgPrzeglady[2, e.RowIndex].Value.ToString();
                fro.przeglad1.nrFAktury = dgPrzeglady[3, e.RowIndex].Value.ToString();
                fro.przeglad1.dataFaktury = dgPrzeglady[4, e.RowIndex].Value.ToString();
                fro.przeglad1.uwagi = dgPrzeglady[7, e.RowIndex].Value.ToString();
                fro.przeglad1.zrobiony = false;
                //dgPrzeglady.Columns;
                fro.przeglad1.idFirma = ID_FIRM;
                fro.przeglad1.nazwaFirmy = NAZWA;
                fro.przeglad1.idTypPrzegladu = Convert.ToInt32(ID_TYP_PRZEDLADU);
                fro.przeglad1.SetDataToControl();
                fro.Disposed += new EventHandler(fro_Disposed);
                fro.ShowDialog();
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1 && e.ColumnIndex != deleteButton.Index)
            {
                PrzegladFOrm fro = new PrzegladFOrm();
                fro.TRYB = PrzegladFOrm.TrybENum.Edycja;
                fro.przeglad1.idPrzeglad = Convert.ToInt32(dataGridView1[0, e.RowIndex].Value.ToString());
                fro.przeglad1.dataPrzegladu = dataGridView1[1, e.RowIndex].Value.ToString();
                //   fro.przeglad1.zrobiony = Convert.ToBoolean(dgPrzeglady[2, e.RowIndex].Value.ToString());
                fro.przeglad1.nrProtokolu = dataGridView1[2, e.RowIndex].Value.ToString();
                fro.przeglad1.nrFAktury = dataGridView1[3, e.RowIndex].Value.ToString();
                fro.przeglad1.dataFaktury = dataGridView1[4, e.RowIndex].Value.ToString();
                fro.przeglad1.uwagi = dataGridView1[5, e.RowIndex].Value.ToString();
                fro.przeglad1.zrobiony = true;
                //dgPrzeglady.Columns;
                fro.przeglad1.idFirma = ID_FIRM;
                fro.przeglad1.nazwaFirmy = NAZWA;
                fro.przeglad1.idTypPrzegladu = Convert.ToInt32(ID_TYP_PRZEDLADU);
                fro.przeglad1.SetDataToControl();
                fro.Disposed += new EventHandler(fro_Disposed);
                fro.ShowDialog();
            }
        }

        private void dgKontakty_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Form1 f1 = new Form1();
            int IDKONTAKTINT = 0;


            int currentRow = int.Parse(e.RowIndex.ToString());
            try
            {
                string IDKONTAKTstr = dgKontakty[0, currentRow].Value.ToString();
                IDKONTAKTINT = int.Parse(IDKONTAKTstr);
            }
            catch (Exception ex) { }
            // edit button
            if (dgKontakty.Columns[e.ColumnIndex] != deleteButton && currentRow >= 0)
            {
                fk = new FormKontakt();
                fk.kontakt1.TRYB = Kontakt.TrybEnum.Edycja;
                fk.kontakt1.IDFirma = ID_FIRM;
                fk.kontakt1.SetCOntrolByStrings(
                    dgKontakty.SelectedRows[0].Cells[1].Value.ToString(),
                    dgKontakty.SelectedRows[0].Cells[2].Value.ToString(),
                    dgKontakty.SelectedRows[0].Cells[3].Value.ToString(),
                    dgKontakty.SelectedRows[0].Cells[4].Value.ToString(),
                    dgKontakty.SelectedRows[0].Cells[5].Value.ToString(),
                    Convert.ToInt32(dgKontakty.SelectedRows[0].Cells[0].Value.ToString()));

                fk.Disposed += new EventHandler(fk_Disposed);
                fk.ShowDialog();

            }
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

    }
}